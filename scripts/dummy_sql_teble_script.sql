-- drop database if exists timeTracker;

create database timeTracker;

use timeTracker;

create table egw_timesheet (ts_id int NOT NULL PRIMARY KEY AUTO_INCREMENT, ts_title text, ts_start long, ts_duration int, ts_quantity double, ts_owner int, ts_modified long, ts_modifier int);

create table egw_infolog (info_id int NOT NULL PRIMARY KEY AUTO_INCREMENT,info_subject text, info_status text, info_des text, info_responsible text, info_owner int, info_datemodified long, info_duedate long, info_modifier text);

create table egw_links (link_app1 text,link_id1 int,link_app2 text, link_id2 int, link_lastmod long,link_owner int);

create table egw_accounts (account_id int NOT NULL PRIMARY KEY AUTO_INCREMENT, account_lid text);

insert into egw_accounts (account_id, account_lid) VALUES (30, 'roy');
insert into egw_accounts (account_id, account_lid) VALUES (31, 'rocker');
insert into egw_accounts (account_id, account_lid) VALUES (32, 'focker');

INSERT INTO egw_infolog (info_subject,info_status,info_des,info_responsible,info_owner,info_datemodified,info_duedate) VALUES ('FOXTROT','programmieren','Blablabla','30,31',30,UNIX_TIMESTAMP() - (60*60*24*5),UNIX_TIMESTAMP() + (60*60*24*2));
INSERT INTO egw_infolog (info_subject,info_status,info_des,info_responsible,info_owner,info_datemodified,info_duedate) VALUES ('UNIFORM','programmieren','Blablabla','30,32',30,UNIX_TIMESTAMP() - (60*60*24*5),UNIX_TIMESTAMP() + (60*60*24*5));
INSERT INTO egw_infolog (info_subject,info_status,info_des,info_responsible,info_owner,info_datemodified,info_duedate) VALUES ('CHARLIE','programmieren','Blablabla','30,31,32',30,UNIX_TIMESTAMP() - (60*60*24*5),UNIX_TIMESTAMP() + (60*60*24*10));
INSERT INTO egw_infolog (info_subject,info_status,info_des,info_responsible,info_owner,info_datemodified,info_duedate) VALUES ('KILO','programmieren','Blablabla','30',30,UNIX_TIMESTAMP() - (60*60*24*5), UNIX_TIMESTAMP() - (60*60*24*1));

create table egw_status (
	id int PRIMARY KEY AUTO_INCREMENT,
	text VARCHAR(100));

INSERT INTO egw_status (text) VALUES ('anayse');
INSERT INTO egw_status (text) VALUES ('programmieren');
INSERT INTO egw_status (text) VALUES ('testen');
INSERT INTO egw_status (text) VALUES ('bereit für neue Version');
INSERT INTO egw_status (text) VALUES ('installation');
INSERT INTO egw_status (text) VALUES ('installation schulung');
INSERT INTO egw_status (text) VALUES ('installation produktion');
INSERT INTO egw_status (text) VALUES ('warten auf Antwort von Kunde');
INSERT INTO egw_status (text) VALUES ( 'ongoing');
INSERT INTO egw_status (text) VALUES ( 'verrechnen');


  SELECT * FROM egw_accounts WHERE (FIND_IN_SET(egw_accounts.account_id,
  	(SELECT egw_infolog.info_responsible FROM  egw_infolog WHERE egw_infolog.info_id = 2 LIMIT 1)
  	)) > 0
/*
-- INSERT_TASK*
INSERT INTO egw_timesheet (
    ts_title,
    ts_start,
    ts_duration,
    ts_quantity,
    ts_owner,
    ts_modified,
    ts_modifier)
VALUES (
    SUBSTRING(':task.title', 1, 80),
    :schedule.dateAsLong / 1000,
    :schedule.inHoursTimeUsed * 60,
    :schedule.inHoursTimeUsed,
    (    SELECT
            account_id
        FROM
            egw_accounts
        WHERE account_lid = ':user.username'
    ),
    :schedule.dateAsLong / 1000,
    (SELECT account_id FROM egw_accounts WHERE account_lid = ':user.username'))
	 -- wenn infolog kategorie intern, dann wird status auf timesheet auf intern gesetzt sonst projektarbeiten

-------------------------------------------------------------------------------------------------
-- SELECT_TASK*
-- Anayse
SELECT
  info_id AS idExport,
  info_subject AS title,
  info_status AS status,
  CAST(info_duedate * 1000 AS UNSIGNED) AS dueDate,
  info_des as description,
  (SELECT GROUP_CONCAT(DISTINCT egw_accounts.account_lid) FROM egw_accounts WHERE (FIND_IN_SET(egw_accounts.account_id,
	  (SELECT nested_infolog.info_responsible FROM  egw_infolog as nested_infolog WHERE nested_infolog.info_id = egw_infolog.info_id LIMIT 1)
	)) > 0) AS relatedUsers
FROM
  egw_infolog
WHERE
	egw_infolog.info_responsible LIKE (SELECT CONCAT('%',account_id,'%') FROM egw_accounts WHERE account_lid = 'roy')
	AND info_status = 'anayse';

-- Programmieren
SELECT
  info_id AS idExport,
  info_subject AS title,
  info_status AS status,
  CAST(info_duedate * 1000 AS UNSIGNED) AS dueDate,
  info_des as description,
  (SELECT GROUP_CONCAT(DISTINCT egw_accounts.account_lid) FROM egw_accounts WHERE (FIND_IN_SET(egw_accounts.account_id,
	  (SELECT nested_infolog.info_responsible FROM  egw_infolog as nested_infolog WHERE nested_infolog.info_id = egw_infolog.info_id LIMIT 1)
	)) > 0) AS relatedUsers
FROM
  egw_infolog
WHERE
	egw_infolog.info_responsible LIKE (SELECT CONCAT('%',account_id,'%') FROM egw_accounts WHERE account_lid = 'roy')
	AND info_status = 'programmieren';
-- Testen
SELECT
  info_id AS idExport,
  info_subject AS title,
  info_status AS status,
  CAST(info_duedate * 1000 AS UNSIGNED) AS dueDate,
  info_des as description,
  (SELECT GROUP_CONCAT(DISTINCT egw_accounts.account_lid) FROM egw_accounts WHERE (FIND_IN_SET(egw_accounts.account_id,
	  (SELECT nested_infolog.info_responsible FROM  egw_infolog as nested_infolog WHERE nested_infolog.info_id = egw_infolog.info_id LIMIT 1)
	)) > 0) AS relatedUsers
FROM
  egw_infolog
WHERE
	egw_infolog.info_responsible LIKE (SELECT CONCAT('%',account_id,'%') FROM egw_accounts WHERE account_lid = 'roy')
	AND info_status = 'testen';
-- bereit für Version
SELECT
  info_id AS idExport,
  info_subject AS title,
  info_status AS status,
  CAST(info_duedate * 1000 AS UNSIGNED) AS dueDate,
  info_des as description,
  (SELECT GROUP_CONCAT(DISTINCT egw_accounts.account_lid) FROM egw_accounts WHERE (FIND_IN_SET(egw_accounts.account_id,
	  (SELECT nested_infolog.info_responsible FROM  egw_infolog as nested_infolog WHERE nested_infolog.info_id = egw_infolog.info_id LIMIT 1)
	)) > 0) AS relatedUsers
FROM
  egw_infolog
WHERE
	egw_infolog.info_responsible LIKE (SELECT CONCAT('%',account_id,'%') FROM egw_accounts WHERE account_lid = 'roy')
	AND info_status = 'bereit für neue Version';
-- Installation planen
SELECT
  info_id AS idExport,
  info_subject AS title,
  info_status AS status,
  CAST(info_duedate * 1000 AS UNSIGNED) AS dueDate,
  info_des as description,
  (SELECT GROUP_CONCAT(DISTINCT egw_accounts.account_lid) FROM egw_accounts WHERE (FIND_IN_SET(egw_accounts.account_id,
	  (SELECT nested_infolog.info_responsible FROM  egw_infolog as nested_infolog WHERE nested_infolog.info_id = egw_infolog.info_id LIMIT 1)
	)) > 0) AS relatedUsers
FROM
  egw_infolog
WHERE
	egw_infolog.info_responsible LIKE (SELECT CONCAT('%',account_id,'%') FROM egw_accounts WHERE account_lid = 'roy')
	AND info_status = 'installation';
-- Installiert (Schulung)
SELECT
  info_id AS idExport,
  info_subject AS title,
  info_status AS status,
  CAST(info_duedate * 1000 AS UNSIGNED) AS dueDate,
  info_des as description,
  (SELECT GROUP_CONCAT(DISTINCT egw_accounts.account_lid) FROM egw_accounts WHERE (FIND_IN_SET(egw_accounts.account_id,
	  (SELECT nested_infolog.info_responsible FROM  egw_infolog as nested_infolog WHERE nested_infolog.info_id = egw_infolog.info_id LIMIT 1)
	)) > 0) AS relatedUsers
FROM
  egw_infolog
WHERE
	egw_infolog.info_responsible LIKE (SELECT CONCAT('%',account_id,'%') FROM egw_accounts WHERE account_lid = 'roy')
	AND info_status = 'installation Schulung';
-- Installiert (Produktion)
SELECT
  info_id AS idExport,
  info_subject AS title,
  info_status AS status,
  CAST(info_duedate * 1000 AS UNSIGNED) AS dueDate,
  info_des as description,
  (SELECT GROUP_CONCAT(DISTINCT egw_accounts.account_lid) FROM egw_accounts WHERE (FIND_IN_SET(egw_accounts.account_id,
	  (SELECT nested_infolog.info_responsible FROM  egw_infolog as nested_infolog WHERE nested_infolog.info_id = egw_infolog.info_id LIMIT 1)
	)) > 0) AS relatedUsers
FROM
  egw_infolog
WHERE
	egw_infolog.info_responsible LIKE (SELECT CONCAT('%',account_id,'%') FROM egw_accounts WHERE account_lid = 'roy')
	AND info_status = 'installation produktion';
-- warten auf Kunde
SELECT
  info_id AS idExport,
  info_subject AS title,
  info_status AS status,
  CAST(info_duedate * 1000 AS UNSIGNED) AS dueDate,
  info_des as description,
  (SELECT GROUP_CONCAT(DISTINCT egw_accounts.account_lid) FROM egw_accounts WHERE (FIND_IN_SET(egw_accounts.account_id,
	  (SELECT nested_infolog.info_responsible FROM  egw_infolog as nested_infolog WHERE nested_infolog.info_id = egw_infolog.info_id LIMIT 1)
	)) > 0) AS relatedUsers
FROM
  egw_infolog
WHERE
	egw_infolog.info_responsible LIKE (SELECT CONCAT('%',account_id,'%') FROM egw_accounts WHERE account_lid = 'roy')
	AND info_status = 'warten auf Antwort von Kunde';
-- Andere
SELECT
  info_id AS idExport,
  info_subject AS title,
  info_status AS status,
  CAST(info_duedate * 1000 AS UNSIGNED) AS dueDate,
  info_des as description,
  (SELECT GROUP_CONCAT(DISTINCT egw_accounts.account_lid) FROM egw_accounts WHERE (FIND_IN_SET(egw_accounts.account_id,
	  (SELECT nested_infolog.info_responsible FROM  egw_infolog as nested_infolog WHERE nested_infolog.info_id = egw_infolog.info_id LIMIT 1)
	)) > 0) AS relatedUsers
FROM
  egw_infolog
WHERE
	egw_infolog.info_responsible LIKE (SELECT CONCAT('%',account_id,'%') FROM egw_accounts WHERE account_lid = 'roy')
	AND info_status = 'ongoing';

-------------------------------------------------------------------------------------------------

-- INSERT_TASK_SHEDULE_RELATION*
INSERT INTO egw_links (
    link_app1,
    link_id1,
    link_app2,
    link_id2,
    link_lastmod,
    link_owner)
VALUES (
    'timesheet',
    :insered.schedule.id,
    'infolog',
    :task.idExport,
    :schedule.dateAsLong / 1000,
    (    SELECT
             account_id
         FROM
             egw_accounts
         WHERE account_lid = ':user.username')
);
UPDATE egw_infolog
    SET info_des = (SELECT CONCAT(':schedule.description.summary', '
', info_des)),
    info_modifier = (    SELECT
             account_id
         FROM
             egw_accounts
         WHERE account_lid = ':user.username'),
info_datemodified = (SELECT UNIX_TIMESTAMP(NOW()))
WHERE info_id = :task.idExport

-------------------------------------------------------------------------------------------------

-- SELECT_USER*
select
    account_id as idExport,
    account_lid as username
from
    egw_accounts
ORDER BY username

-------------------------------------------------------------------------------------------------

-- UPDATE_TASK_USER*
UPDATE  egw_infolog
    set info_responsible = (SELECT GROUP_CONCAT(account_id) from egw_accounts WHERE account_lid in (:usernames)),
    info_modifier = (    SELECT
             account_id
         FROM
             egw_accounts
         WHERE account_lid = ':user.username'),
info_datemodified = (SELECT UNIX_TIMESTAMP(NOW()))
WHERE info_id = :idExport

-------------------------------------------------------------------------------------------------
-- UPDATE_TASK_STATUS*
UPDATE egw_infolog
    set info_status = ':task.status'
WHERE info_id = :idExport

-------------------------------------------------------------------------------------------------
-- SELECT_STATUS*
SELECT DISTINCT
    text AS status
FROM
    egw_status
ORDER BY text

-------------------------------------------------------------------------------------------------
-- SEARCH_TASK*
-- Suche
SELECT
	info_id AS idExport,
	info_subject AS title,
	info_status AS status,
	CAST(info_duedate * 1000 AS UNSIGNED) AS dueDate,
	info_des as description
FROM
	egw_infolog
WHERE
	info_id LIKE '%:search%' OR
	info_subject LIKE '%:search%' OR
	info_status LIKE '%:search%' OR
	info_des LIKE '%:search%';

-------------------------------------------------------------------------------------------------
-- UPDATE_TASK_DUEDATE
UPDATE egw_infolog SET info_duedate = UNIX_TIMESTAMP(STR_TO_DATE(':task.dueDate', '%Y-%m-%d')) WHERE info_id = :task.idExport
 */