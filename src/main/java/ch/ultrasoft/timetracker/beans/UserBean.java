package ch.ultrasoft.timetracker.beans;

import java.io.IOException;
import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ch.ultrasoft.timetracker.model.User;

@SessionScoped
@Named("userBean")
public class UserBean implements Serializable {

	private static final long serialVersionUID = -7321752471987710917L;
	private static final Logger LOGGER = Logger.getLogger(UserBean.class);
	private User user;
	
	public void logout() {
		if (FacesContext.getCurrentInstance() != null 
				&& FacesContext.getCurrentInstance().getExternalContext() != null
				&& FacesContext.getCurrentInstance().getExternalContext().getSessionMap() != null){
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().clear();
			LOGGER.debug("Logged out");
			FacesContext.getCurrentInstance().getApplication().getNavigationHandler().handleNavigation(FacesContext.getCurrentInstance(), null, "/");
		} else {
			LOGGER.error("logout failed");
		}
	}

	public User getUser() {
		return user;
	}
	
	public void setUser(User user) {
		this.user = user;
	}
}
