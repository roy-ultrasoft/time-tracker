package ch.ultrasoft.timetracker.converter;

import java.io.Serializable;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;

import org.apache.log4j.Logger;

import ch.ultrasoft.timetracker.interfaces.Keyable;
import ch.ultrasoft.timetracker.model.AbstractEntity;
import ch.ultrasoft.timetracker.service.GenericService;

public class AbstractEntityConverter<T extends AbstractEntity> implements Converter, Serializable {

	private static final long serialVersionUID = 6323745040996457031L;

	private static final Logger LOGGER = Logger.getLogger(AbstractEntityConverter.class);

	@Inject
	private GenericService<T> service;

	private final Class<T> clazz = getGenericType();

	public AbstractEntityConverter() {
	}
	
	public void resetConverter() {
		service.getEm().clear();
	}

	@Override
	public final Object getAsObject(FacesContext arg0, UIComponent arg1, String arg2) {
		try {
			return service.getEm().find(clazz, Long.valueOf(arg2));
		} catch (NumberFormatException e) {
			LOGGER.error(e);
			return null;
		}
	}

	@Override
	public final String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
		if (arg2 instanceof Keyable)
			return ((Keyable) arg2).getId().toString();
		return null;
	}

	@SuppressWarnings("unchecked")
	private Class<T> getGenericType() {
		return (Class<T>) AbstractEntity.class;
	}
}
