package ch.ultrasoft.timetracker.interfaces;

public interface Keyable {

	abstract Long getId();
}
