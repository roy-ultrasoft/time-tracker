package ch.ultrasoft.timetracker.filter;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ch.ultrasoft.timetracker.controller.UserController;
import ch.ultrasoft.timetracker.utils.CryptoUtil;

/**
 * Servlet Filter implementation class UserFilter
 */
@WebFilter("/pages/*")
public class UserFilter implements Filter {

	private final static Logger LOGGER = Logger.getLogger(UserFilter.class);

	@Inject
	private UserController userController;
	
	public UserFilter() {
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		try {
			HttpServletRequest reqt = (HttpServletRequest) request;
			HttpServletResponse resp = (HttpServletResponse) response;
			HttpSession ses = reqt.getSession();
			if (ses.getAttribute("jsf-user") != null || authenticate(reqt, resp, "jsf-user")) {
				chain.doFilter(request, response);
			} else {
				resp.sendRedirect(reqt.getContextPath() + "/login.html");
			}
		} catch (Exception e) {
			LOGGER.error("Error", e);
		}
	}
	
	private boolean authenticate(HttpServletRequest reqt, HttpServletResponse resp, String sessionAttr) throws IOException {
		String username = null;
		String pass = null;
		if (reqt.getParameterMap().containsKey("username"))
			username = reqt.getParameter("username");
		if (reqt.getParameterMap().containsKey("pass"))
			pass = CryptoUtil.toMd5Hash(reqt.getParameter("pass"));
		if (userController.validateLogin(username, pass)) {
				((HttpServletRequest)reqt).getSession().setAttribute(sessionAttr, true);
				resp.sendRedirect(reqt.getContextPath() + "/pages/views/single.jsf");
				return true;
		}
		return false;
	}
	
	@Override
	public void destroy() {

	}
}
