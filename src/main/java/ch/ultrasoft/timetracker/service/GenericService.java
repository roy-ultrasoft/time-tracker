package ch.ultrasoft.timetracker.service;

import java.io.Serializable;
import java.security.Key;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import org.apache.log4j.Logger;

import ch.ultrasoft.timetracker.beans.UserBean;
import ch.ultrasoft.timetracker.interfaces.Keyable;
import ch.ultrasoft.timetracker.model.AbstractEntity;
import ch.ultrasoft.timetracker.persistence.DataRepository;

public class GenericService<T> implements Serializable {

	private static final long serialVersionUID = 7271780038429910478L;

	private final static Logger LOGGER = Logger.getLogger(GenericService.class);

	@Inject
	@DataRepository
	private EntityManager em;
	
	@Inject
	private UserBean userBean;
	
	private EntityTransaction tx;

	public GenericService() {
	}

	public GenericService(EntityManager em) {
		this.em = em;
	}

	public Object save(Object o) throws Exception {
		beforeSave(o);
		tx = em.getTransaction();
		try {
			tx.begin();
			o = em.merge(o);
			em.flush();
			tx.commit();
			LOGGER.debug("merge SUCCESS");
		} catch (Exception e) {
			if (tx.isActive())
				tx.rollback();
			LOGGER.error("merge FAIL:", e);
			throw e;
		}
		return o;
	}

	public Object delete(Object o) throws Exception {
		tx = em.getTransaction();
		try {
			tx.begin();
			if (o instanceof Keyable)
				o = em.find(o.getClass(), ((Keyable)o).getId());
			em.remove(em.contains(o) ? o : em.merge(o));
			em.flush();
			tx.commit();
			LOGGER.debug("delete SUCCESS");
		} catch (Exception e) {
			if (tx.isActive())
				tx.rollback();
			LOGGER.error("delete FAIL:", e);
			throw e;
		}
		return o;
	}

	public void delete(List<Keyable> o) throws Exception {
		tx = em.getTransaction();
		try {
			tx.begin();
			for (Keyable k: o) {
				o = em.find(o.getClass(), ((Keyable)o).getId());
				em.remove(em.contains(o) ? o : em.merge(o));
			}

			em.flush();
			tx.commit();
			LOGGER.debug("delete SUCCESS");
		} catch (Exception e) {
			if (tx.isActive())
				tx.rollback();
			LOGGER.error("delete FAIL:", e);
			throw e;
		}
	}
	
	private void beforeSave(Object o) {
		if (o instanceof AbstractEntity){
			if (((AbstractEntity) o).getId() == null)
				((AbstractEntity) o).setCreator(userBean.getUser().getUsername());
			((AbstractEntity) o).setEditDate(new Date());
		}
	}

	public EntityManager getEm() {
		return em;
	}
}
