package ch.ultrasoft.timetracker.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;

import ch.ultrasoft.timetracker.interfaces.Keyable;
import ch.ultrasoft.timetracker.model.AbstractEntity;

@Entity
public class Query extends AbstractEntity implements Keyable{

	private static final long serialVersionUID = -1968506310371095123L;
	
	public static enum Trigger {
		SELECT_TASK, 
		INSERT_TASK, 
		INSERT_TASK_SHEDULE_RELATION, 
		SELECT_USER, 
		UPDATE_TASK_USER,
		UPDATE_TASK_STATUS,
		SELECT_STATUS,
		SEARCH_TASK, 
		HISTORY_TASK, 
		HISTORY_TASK_USER, 
		HISTORY_TASK_STATUS,
		UPDATE_TASK_DUEDATE
	}
		
	@Column
	private String title;

	@Column
	@Lob
	private String query;

	@Column
	private Trigger trigger;

	@Column
	private Boolean active = true;
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}
	
	public Trigger getTrigger() {
		return trigger;
	}

	public void setTrigger(Trigger trigger) {
		this.trigger = trigger;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	@Override
	public String toString() {
		return "Query [title=" + title + ", query=" + query + ", trigger=" + trigger + ", active=" + active + "]";
	}
}
