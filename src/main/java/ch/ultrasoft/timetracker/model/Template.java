package ch.ultrasoft.timetracker.model;

import ch.ultrasoft.timetracker.interfaces.Keyable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import java.io.Serializable;


@Entity
public class Template extends AbstractEntity implements Serializable, Keyable {

    private static final long serialVersionUID = -1968506310371095123L;

    @Column
    private String title;

    @Column
    @Lob
    private String content;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
