package ch.ultrasoft.timetracker.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

import ch.ultrasoft.timetracker.interfaces.Keyable;

@Entity
public class Schedule extends AbstractEntity implements Keyable {

	private static final long serialVersionUID = 1L;

	@Column
	private String title;

	@Column
	private Date date;

	@Column
	private int timeUsed;

	@Column
	private int timeExported;
	
	@Column
	private boolean exported = false;

	@ManyToOne(cascade=CascadeType.MERGE)
	private Task task;

	@Column
	@Lob
	private String description;

	public Task getTask() {
		return task;
	}

	public void setTask(Task task) {
		this.task = task;
	}

	public boolean isExported() {
		return exported;
	}

	public void setExported(boolean exported) {
		this.exported = exported;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getTimeUsed() {
		return timeUsed;
	}

	public void setTimeUsed(int timeUsed) {
		this.timeUsed = timeUsed;
	}

	public int getTimeExported() {
		return timeExported;
	}

	public void setTimeExported(int timeExported) {
		this.timeExported = timeExported;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "Schedule [title=" + title + ", date=" + date + ", timeUsed=" + timeUsed + ", task=" + task
				+ ", getId()=" + getId() + "]";
	}
}
