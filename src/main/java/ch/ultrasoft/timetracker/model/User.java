package ch.ultrasoft.timetracker.model;

import javax.persistence.Column;
import javax.persistence.Entity;

import ch.ultrasoft.timetracker.utils.CryptoUtil;

import java.util.Objects;

@Entity
public class User extends AbstractEntity {

	private static final long serialVersionUID = 8072488920330467248L;
		
	@Column(unique=true)
	private String username;
	
	@Column
	private String kuerzel;

	@Column
	private String pass;

	@Column
	private String aktuellerSprint;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getKuerzel() {
		return kuerzel;
	}

	public void setKuerzel(String kuerzel) {
		this.kuerzel = kuerzel;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {

		if (pass != null && !Objects.equals(this.pass, pass))
			this.pass = CryptoUtil.toMd5Hash(pass);
	}

	public String getAktuellerSprint() {
		return aktuellerSprint;
	}

	public void setAktuellerSprint(String aktuellerSprint) {
		this.aktuellerSprint = aktuellerSprint;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		return prime * ((username == null) ? 0 : username.hashCode());
	}

	@Override
	public boolean equals(Object obj) {
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "User [username=" + username + ", pass=" + pass + "]";
	}

}