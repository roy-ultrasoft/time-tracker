package ch.ultrasoft.timetracker.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;

import ch.ultrasoft.timetracker.interfaces.Keyable;

@Entity
public class Task extends AbstractEntity implements Serializable, Keyable {

	private static final long serialVersionUID = -1968506310371095123L;

	@Column
	private String title;

	@Column
	@Lob
	private String description;

	@Column
	private String idExport;

	@Column
	private String status;

	@Column
	private Date dueDate;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getIdExport() {
		return idExport;
	}

	public void setIdExport(String idExport) {
		this.idExport = idExport;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	@Override
	public String toString() {
		return "Task [title=" + title + ", description=" + description + ", idExport=" + idExport + ", getId()="
				+ getId() + ", dueDate=" + dueDate + "]";
	}
}
