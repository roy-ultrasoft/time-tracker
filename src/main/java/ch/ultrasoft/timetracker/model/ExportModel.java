package ch.ultrasoft.timetracker.model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import ch.ultrasoft.timetracker.beans.UserBean;
import ch.ultrasoft.timetracker.controller.ScheduleController;

public class ExportModel implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private ScheduleController scheduleController;

	private UserBean userBean;

	private Date date;
	private List<Schedule> schedules = new ArrayList<>();
	private Schedule allInOne = null;
	private String description = null;
	private SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");

	public String getDescription() {
		if (description == null) {
			description = "";
			for (int i = schedules.size() - 1; i >= 0; i--) {
				if ((schedules.get(i).getTitle() != null && !schedules.get(i).getTitle().isEmpty())
						|| (schedules.get(i).getDescription() != null && !schedules.get(i).getDescription().isEmpty())) {
					description += dateFormat.format(date) + " " + userBean.getUser().getKuerzel() + ": "
							+ (schedules.get(i).getTitle() != null ? schedules.get(i).getTitle() + "\n" : "")
							+ (schedules.get(i).getDescription() != null ? schedules.get(i).getDescription() + "\n***" : "***");}
			}
		}
		return description;
	}

	public ExportModel(Date date, UserBean userBean) {
		this.date = date;
		this.userBean = userBean;
	}

	public Schedule getAllInOne() {
		if (allInOne == null) {
			int timeUsed = 0;
			for (Schedule s : schedules) {
				allInOne = s;
				timeUsed += s.getTimeUsed();
			}
			allInOne.setTimeUsed(timeUsed);
		}
		return allInOne;
	}

	public double getTimeInHours() {
		return Math.round(((getAllInOne().getTimeUsed() * 4) / 60.) / 60.) / 4.;
	}

	public void setTimeInHours(double timeUsed) {
		getAllInOne().setTimeUsed((int) (timeUsed * 60 * 60));
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public List<Schedule> getSchedules() {
		return schedules;
	}

	public void setSchedules(List<Schedule> schedules) {
		this.schedules = schedules;
	}

}
