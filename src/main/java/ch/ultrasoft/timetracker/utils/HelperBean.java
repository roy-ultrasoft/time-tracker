package ch.ultrasoft.timetracker.utils;

import java.io.Serializable;
import java.util.Date;

import javax.inject.Named;

@Named("helperBean")
public class HelperBean implements Serializable {

	public static Date longToDate(Long l) {
		return new Date(l);
	}
	
	public long daysToLate(Long l) {
		return ((new Date().getTime()) - l) / 1000 / 60 / 60 / 24;
	}
}
