package ch.ultrasoft.timetracker.utils;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.log4j.Logger;

public final class CryptoUtil {
	private final static Logger LOGGER = Logger.getLogger(CryptoUtil.class);

	public static String toMd5Hash(String str) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(str.getBytes());

			byte byteData[] = md.digest();

			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < byteData.length; i++) {
				sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
			}
			StringBuffer hexString = new StringBuffer();
			for (int i = 0; i < byteData.length; i++) {
				String hex = Integer.toHexString(0xff & byteData[i]);
				if (hex.length() == 1)
					hexString.append('0');
				hexString.append(hex);
			}
			if (LOGGER.isDebugEnabled())
				LOGGER.debug("Hash generated: " + hexString.toString());
			return hexString.toString();
		} catch (NoSuchAlgorithmException e) {
			LOGGER.error("MD5 hashing failed", e);
			return null;
		}
	}
}
