package ch.ultrasoft.timetracker.utils;

import java.io.Closeable;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import ch.ultrasoft.timetracker.model.ColumnModel;

public class SQLUtil implements Closeable {

	private static final Logger LOGGER = Logger.getLogger(SQLUtil.class);
	private static final Pattern QUERY_TITLE_REGEX = Pattern.compile("^\\s*--\\s*(.+)");
	private static Connection conn = null;
	private String jdbcUrl;
	private String user;
	private String pass;
	private static SQLUtil sqlUtil = null;
	
	private SQLUtil() throws Exception {
		Properties properties = new Properties();
		try {
			properties.loadFromXML(new FileInputStream("src/main/resources/params.xml"));
			this.jdbcUrl = properties.getProperty("url");
			this.pass = properties.getProperty("password");
			this.user = properties.getProperty("user");
			buildConnection();
		} catch (IOException | SQLException e) {
			LOGGER.error("could not read credentials from xml resource");
			throw e;
		}
	}
	
	public static SQLUtil createInstance() throws Exception {
		if (sqlUtil == null || conn == null || conn.isClosed())
			sqlUtil = new SQLUtil();
		return sqlUtil;
	}

	private void buildConnection() throws SQLException {
		conn = DriverManager.getConnection(jdbcUrl, user, pass);
	}

	public Map<String, List<Object>> sqlToTasks(String sql) throws SQLException {
		ResultSet rs = null;
		Map<String, List<Object>> map = new HashMap<>();
		List<Object> data = new ArrayList<>();
		List<Object> columns = new ArrayList<>();
		List<Object> title = new ArrayList<>();
		Matcher m = QUERY_TITLE_REGEX.matcher(sql);
		if (m.find() && m.groupCount() > 0) {
			title.add(m.group(1));
		}
		Statement statement = null;
		try {
			statement = createStatement();
			rs = statement.executeQuery(sql);

			for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
				String key = rs.getMetaData().getColumnLabel(i);
				columns.add(new ColumnModel(key, key));
			}
			while (rs.next()) {
				Map<String, Object> dataMap = new TreeMap<>();
				for (Object s : columns) {
					dataMap.put(((ColumnModel) s).getProperty(), rs.getObject(((ColumnModel) s).getProperty()));
				}
				data.add(dataMap);
			}
			// conn.commit();
			map.put("columns", columns);
			map.put("data", data);
			map.put("title", title);
			return map;
		} catch (SQLException e) {
			LOGGER.error("executeQuery failed: QUERY=" + sql, e);
			// conn.rollback();
			throw e;
		} finally {
			if (rs != null && !rs.isClosed())
			rs.close();
			if (statement != null)
				statement.close();
		}
	}

	public int executeUpdate(String sql) throws SQLException {
		Statement statement = null;
		try {
			statement = conn.createStatement();
			statement.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);

			ResultSet rs = statement.getGeneratedKeys();
			int key = 0;
			if (rs.next()) {
				// Retrieve the auto generated key(s).
				key = rs.getInt(1);
			}
			return key;
			// conn.commit();
		} catch (SQLException e) {
			LOGGER.error("executeUpdate failed: QUERY=" + sql, e);
			// conn.rollback();
			throw e;
		} finally {
			if (statement != null)
				statement.close();
		}
	}

	public ResultSet executeQuery(String sql) throws SQLException {
		Statement statement = null;
		try {
			statement = createStatement();
			return statement.executeQuery(sql);
			// conn.commit();
		} catch (SQLException e) {
			LOGGER.error("execute query failed: QUERY=" + sql, e);
			// conn.rollback();
			throw e;
		}
	}

	private Statement createStatement() throws SQLException {
		return getConection().createStatement();
	}

	private Connection getConection() throws SQLException {
		if (conn == null || conn.isClosed())
			buildConnection();
		return conn;
	}

	private void closeConection() throws SQLException {
		if (conn != null && conn.isClosed())
			conn.close();
	}

	@Override
	public void close() throws IOException {
		try {
			closeConection();
		} catch (SQLException e) {
			LOGGER.error("failed to close connection", e);
			throw new IOException(e);
		}

	}
}
