package ch.ultrasoft.timetracker.controller;

import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import org.apache.log4j.Logger;

import ch.ultrasoft.timetracker.model.User;

@Named("userController")
@SessionScoped
public class UserController extends AbstractCrudController<User> {

	private static final long serialVersionUID = 7128810771691555739L;
	private static final Logger LOGGER = Logger.getLogger(UserController.class);
	
	public UserController(Class<User> clazz) {
		super(clazz);
	}

	public UserController() {
		this(User.class);
	}

	public boolean validateLogin(String username, String pass) {
		initRecords();
		if (username != null && pass != null) {
			List<User> users = getService().getEm().createQuery("FROM User WHERE username = :username AND pass = :pass", User.class)
					.setParameter("username", username).setParameter("pass", pass).getResultList();
			if (users != null && !users.isEmpty()) {
				userBean.setUser(users.get(0));
				LOGGER.info("LOGGED IN username: " + userBean.getUser().getUsername());
				return true;
			}
		}
		return false;
	}
	
	public void save(User u) {
		setSelectedRecord(u);
		saveSelected();
	}

}