package ch.ultrasoft.timetracker.controller;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import ch.ultrasoft.timetracker.model.Query;

@Named("queryController")
@ViewScoped
public class QueryController extends AbstractCrudController<Query> {

	private static final long serialVersionUID = 7128810771691555798L;

	private Query selectTasksQuery;
	private Query insertTaskQuery;
	private Query insertTaskScheduleRelationQuery;
	private Query selectUsersQuery;
	private Query updateTaskUserQuery;
	private Query updateTaskStatusQuery;
	private Query selectStatusQuery;
	private Query searchTaskQuery;
	private Query historyTaskQuery;
	private Query historyTaskUserQuery;
	private Query historyTaskStatusQuery;
	private Query updateTaskDueDateQuery;
	
	public QueryController() {
		this(Query.class);
	}
	
	public QueryController(Class<Query> clazz) {
		super(clazz);
	}
	
	@PostConstruct
	public void initQueries() {
		for (Query q : getRecords()) {
			if (q.getTrigger().equals(Query.Trigger.SELECT_TASK))
				selectTasksQuery = q;
			else if (q.getTrigger().equals(Query.Trigger.INSERT_TASK))
				insertTaskQuery = q;
			else if (q.getTrigger().equals(Query.Trigger.INSERT_TASK_SHEDULE_RELATION))
				insertTaskScheduleRelationQuery = q;
			else if (q.getTrigger().equals(Query.Trigger.SELECT_USER))
				selectUsersQuery = q;
			else if (q.getTrigger().equals(Query.Trigger.UPDATE_TASK_USER))
				updateTaskUserQuery = q;
			else if (q.getTrigger().equals(Query.Trigger.UPDATE_TASK_STATUS))
				updateTaskStatusQuery = q;
			else if (q.getTrigger().equals(Query.Trigger.SELECT_STATUS))
				selectStatusQuery = q;
			else if (q.getTrigger().equals(Query.Trigger.SEARCH_TASK))
				searchTaskQuery = q;
			else if (q.getTrigger().equals(Query.Trigger.HISTORY_TASK))
				historyTaskQuery = q;
			else if (q.getTrigger().equals(Query.Trigger.HISTORY_TASK_USER))
				historyTaskUserQuery = q;
			else if (q.getTrigger().equals(Query.Trigger.HISTORY_TASK_STATUS))
				historyTaskStatusQuery = q;
			else if (q.getTrigger().equals(Query.Trigger.UPDATE_TASK_DUEDATE))
				updateTaskDueDateQuery = q;
		}
	}
	
	public void save(Query q) {
		setSelectedRecord(q);
		saveSelected();
	}

	public Query getSelectTasksQuery() {
		return selectTasksQuery;
	}

	public void setSelectTasksQuery(Query selectTasksQuery) {
		this.selectTasksQuery = selectTasksQuery;
	}

	public Query getInsertTaskQuery() {
		return insertTaskQuery;
	}

	public void setInsertTaskQuery(Query insertTaskQuery) {
		this.insertTaskQuery = insertTaskQuery;
	}

	public Query getInsertTaskScheduleRelationQuery() {
		return insertTaskScheduleRelationQuery;
	}

	public void setInsertTaskScheduleRelationQuery(Query insertTaskScheduleRelationQuery) {
		this.insertTaskScheduleRelationQuery = insertTaskScheduleRelationQuery;
	}
	
	public Query getSelectUsersQuery() {
		return selectUsersQuery;
	}

	public void setSelectUsersQuery(Query selectUsersQuery) {
		this.selectUsersQuery = selectUsersQuery;
	}
	
	public Query getUpdateTaskUserQuery() {
		return updateTaskUserQuery;
	}

	public void setUpdateTaskUserQuery(Query updateTaskUserQuery) {
		this.updateTaskUserQuery = updateTaskUserQuery;
	}

	public Query getUpdateTaskStatusQuery() {
		return updateTaskStatusQuery;
	}

	public void setUpdateTaskStatusQuery(Query updateTaskStatusQuery) {
		this.updateTaskStatusQuery = updateTaskStatusQuery;
	}
	
	public Query getSelectStatusQuery() {
		return selectStatusQuery;
	}

	public void setSelectStatusQuery(Query selectStatusQuery) {
		this.selectStatusQuery = selectStatusQuery;
	}

	public Query getSearchTaskQuery() {
		return this.searchTaskQuery;
	}
	
	public void setSearchTaskQuery(Query searchTaskQuery) {
		this.searchTaskQuery = searchTaskQuery;
	}
	
	public Query getHistoryTaskQuery() {
		return this.historyTaskQuery;
	}

	public void setHistoryTaskQuery(Query historyTaskQuery) {
		this.historyTaskQuery = historyTaskQuery;
	}

	public Query getHistoryTaskUserQuery() {
		return historyTaskUserQuery;
	}

	public void setHistoryTaskUserQuery(Query historyTaskUserQuery) {
		this.historyTaskUserQuery = historyTaskUserQuery;
	}
	
	public Query getHistoryTaskStatusQuery() {
		return historyTaskStatusQuery;
	}

	public void setHistoryTaskStatusQuery(Query historyTaskStatusQuery) {
		this.historyTaskStatusQuery = historyTaskStatusQuery;
	}

	public Query getUpdateTaskDueDateQuery() {
		return updateTaskDueDateQuery;
	}

	public void setUpdateTaskDueDateQuery(Query updateTaskDueDateQuery) {
		this.updateTaskDueDateQuery = updateTaskDueDateQuery;
	}

	@Override
	public String toString() {
		return "QueryController [selectTasksQuery=" + selectTasksQuery + "\n\n insertTaskQuery=" + insertTaskQuery
				+ "\n\n insertTaskScheduleRelationQuery=" + insertTaskScheduleRelationQuery + "\n\n selectUsersQuery="
				+ selectUsersQuery + "\n\n updateTaskUserQuery=" + updateTaskUserQuery + "\n\n updateTaskStatusQuery="
				+ updateTaskStatusQuery + "\n\n selectStatusQuery=" + selectStatusQuery + "]";
	}

}
