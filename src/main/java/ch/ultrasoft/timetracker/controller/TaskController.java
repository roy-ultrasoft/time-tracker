package ch.ultrasoft.timetracker.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityTransaction;

import org.apache.log4j.Logger;

import ch.ultrasoft.timetracker.model.ExportModel;
import ch.ultrasoft.timetracker.model.Query;
import ch.ultrasoft.timetracker.model.Schedule;
import ch.ultrasoft.timetracker.model.Task;
import ch.ultrasoft.timetracker.utils.SQLUtil;

@Named("taskController")
@ViewScoped
public class TaskController extends AbstractCrudController<Task> {

    private static final long serialVersionUID = 7128810771691555739L;
    private static final Logger LOGGER = Logger.getLogger(TaskController.class);

    private List<Map<String, List<Object>>> columnsBySQL = new ArrayList<>();
    private List<Object> filteredColumnsBySQL = new ArrayList<>();

    private int seconds;
    private Schedule schedule = new Schedule();

    private Map<String, ExportModel> toExport = new TreeMap<>();

    @Inject
    private ScheduleController scheduleController;

    @Inject
    private QueryController queryController;

    public TaskController() {
        this(Task.class);
    }

    public TaskController(Class<Task> clazz) {
        super(clazz);
    }

    @PostConstruct
    public void init() {
        initRecords();
    }

    public void incrementSecond() {
        seconds++;
    }

    @Override
    public void createRecord() {
        super.createRecord();
        initRelatedSchedules();
    }

    @Override
    public void saveSelected() {
        schedule.setTimeUsed(schedule.getTimeUsed() + seconds);
        schedule.setDate(new Date());
        try {
            schedule.setTask(getSelectedRecord());
            schedule = (Schedule) getService().save(schedule);
            setSelectedRecord(schedule.getTask());
            initRelatedSchedules();
            drawFacesInfoMessage("Record saved", "");
        } catch (Exception e) {
            LOGGER.error("Failed to save record", e);
            drawFacesError("Failed to save record: " + e.getMessage(), "");
        } finally {
            seconds = 0;
            schedule = new Schedule();
            initRecords();
        }
    }

    public void setSelectedRecord(Map<String, Object> map) {
        Task t = new Task();
        t.setIdExport(map.get("idExport").toString());
        setSelectedRecord(t);
        try {
            getSelectedRecord().setTitle(map.get("title").toString());
        } catch (Exception e) {
            LOGGER.error("title not found from IMPORT_TASKS query");
        }
        try {
            getSelectedRecord().setDescription(map.get("description").toString());
        } catch (Exception e) {
            LOGGER.error("description not found from IMPORT_TASKS query");
        }
        try {
            getSelectedRecord().setDueDate(map.get("dueDate").toString().trim().equals("0") ? null : new Date(Math.round(Double.valueOf(map.get("dueDate").toString()))));
        } catch (Exception e) {
            LOGGER.error("dueDate (long) not found from IMPORT_TASKS query");
        }
        try {
            getSelectedRecord().setStatus(map.get("status").toString());
        } catch (Exception e) {
            LOGGER.error("status not found from IMPORT_TASKS query");
        }
    }

    @Override
    public void setSelectedRecord(Task selectedRecord) {
        seconds = 0;
        if (selectedRecord != null && selectedRecord.getId() == null && selectedRecord.getIdExport() != null)
            for (Task t : getRecords()) {
                if (t != null && t.getIdExport() != null && t.getIdExport().equals(selectedRecord.getIdExport())) {
                    selectedRecord = t;
                    break;
                }
            }
        super.setSelectedRecord(selectedRecord);
        initRelatedSchedules();
        LOGGER.debug("setSelectedRecord: " + selectedRecord);
    }

    private void initRelatedSchedules() {
        if (scheduleController != null)
            scheduleController.initRelatedSchedules(getSelectedRecord());
    }

    @Override
    public void deleteSelected() {
        initRelatedSchedules();
        try {
            for (Schedule s : scheduleController.getRelatedSchedules())
                getService().delete(s);
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.deleteSelected();
    }

    public void deleteExported() {
        final List<Long> notToDelete = getService().getEm().createQuery("SELECT DISTINCT s.task.id FROM Schedule s WHERE s.exported = false", Long.class).getResultList();
        try {
            getService().getEm().getTransaction().begin();
            getService().getEm().createQuery("DELETE FROM Schedule s WHERE s.task.id NOT IN (:notToDelete)")
                    .setParameter("notToDelete", notToDelete)
                    .executeUpdate();
            getService().getEm().createQuery("DELETE FROM Task t WHERE t.id NOT IN (:notToDelete)")
                    .setParameter("notToDelete", notToDelete)
                    .executeUpdate();
            getService().getEm().flush();
            getService().getEm().getTransaction().commit();
            drawFacesInfoMessage("Exported record(s) deleted", "");
            initRecords();
        } catch (Exception e) {
            if (getService().getEm().getTransaction().isActive())
                getService().getEm().getTransaction().rollback();
            LOGGER.error("Deleting exported Tasks failed", e);
            drawFacesError("ERROR Deleting exported tasks: " + e.getMessage(), "");
        }
    }

    public void initColumnsBySQL() {
        try {
            SQLUtil sqlService = SQLUtil.createInstance();
            columnsBySQL = new ArrayList<>();
            for (String query : queryController.getSelectTasksQuery().getQuery().split(";")) {
                columnsBySQL.add(sqlService.sqlToTasks(query.replaceAll(":user.username", userBean.getUser().getUsername())));
            }
        } catch (Exception e) {
            LOGGER.error("initColumnsBySQL failed", e);
            drawFacesError("ERROR Importing Tasks: " + e.getMessage(), "");
        }
    }


    public void prepareExport(Map<String, Object> map) {
        setSelectedRecord(map);
        prepareExport(getSelectedRecord());
    }

    public void prepareExport(Task t) {
        if (t != null) {
            setSelectedRecord(t);
            toExport = new TreeMap<>();
            initRelatedSchedules();
            for (Schedule s : scheduleController.getRelatedSchedules()) {
                String date = new SimpleDateFormat("yyyy-MM-dd").format(s.getDate());
                if (!s.isExported()) {
                    if (!toExport.containsKey(date))
                        toExport.put(date, new ExportModel(s.getDate(), userBean));
                    toExport.get(date).getSchedules().add(s);
                }
            }
        }
    }

    private String buildExportQuery(String query, Map.Entry<String, ExportModel> e, int exportedIdSchedule) {
        return query
                .replaceAll(":insered.schedule.id", (exportedIdSchedule + "").replaceAll("'", "''"))
                .replaceAll(":schedule.timeUsed", "" + e.getValue().getAllInOne().getTimeUsed())
                .replaceAll(":task.idExport", e.getValue().getAllInOne().getTask().getIdExport().replaceAll("'", "''"))
                .replaceAll(":task.dueDate", new SimpleDateFormat("yyyy-MM-dd").format(e.getValue().getAllInOne().getTask().getDueDate() != null ? e.getValue().getAllInOne().getTask().getDueDate() : new Date()).replaceAll("'", "''"))
                .replaceAll(":schedule.description.summary", e.getValue().getDescription().replaceAll("'", "''"))
                .replaceAll(":task.title", e.getValue().getAllInOne().getTask().getTitle().replaceAll("'", "''"))
                .replaceAll(":user.username", userBean.getUser().getUsername().replaceAll("'", "''"))
                .replaceAll(":user.sprint", (userBean.getUser().getAktuellerSprint() != null ? userBean.getUser().getAktuellerSprint() : "no spring selected").replaceAll("'", "''"))
                .replaceAll(":schedule.inHoursTimeUsed", (e.getValue().getTimeInHours() + "").replaceAll("'", "''"))
                .replaceAll(":schedule.dateAsLong", (e.getValue().getAllInOne().getDate().getTime() + "").replaceAll("'", "''"))
                .replaceAll(":schedule.dateAsISO", new SimpleDateFormat("yyyy-MM-dd").format(e.getValue().getAllInOne().getDate()).replaceAll("'", "''"));
    }

    public void exportSchedules() {
        List<Schedule> exported = new ArrayList<>();
        Query q = queryController.getInsertTaskQuery();
        try {
            SQLUtil sqlService = SQLUtil.createInstance();
            for (Map.Entry<String, ExportModel> e : toExport.entrySet()) {


                // WRITE HISTORY
                Query qHistory = queryController.getHistoryTaskQuery();

                if (qHistory != null) {
                    String queryHistory = buildExportQuery(qHistory.getQuery(), e, 0);
                    sqlService.executeUpdate(queryHistory);
                }

                int inseredId = 0;
                String query = null;
                if (e.getValue().getTimeInHours() != 0.0) {
                    query = buildExportQuery(q.getQuery(), e, 0);
                    inseredId = sqlService.executeUpdate(query);
                }

                EntityTransaction tx = null;
                try {
                    for (String sql : queryController.getInsertTaskScheduleRelationQuery().getQuery().split(";")) {
                        if (e.getValue().getTimeInHours() == 0.0 && sql.contains(":insered.schedule.id")) {
                            continue;
                        } else {
                            String queryRelate = buildExportQuery(sql, e, inseredId);
                            sqlService.executeUpdate(queryRelate);
                        }
                    }
                    tx = getService().getEm().getTransaction();
                    tx.begin();
                    for (Schedule s : e.getValue().getSchedules()) {
                        s.setExported(true);
                        exported.add(getService().getEm().merge(s));

                    }
                    getService().getEm().flush();
                    tx.commit();

                    if (e.getValue().getTimeInHours() == 0.) {
                        drawFacesInfoMessage("Task exported", "");
                    } else {
                        drawFacesInfoMessage(exported.size() + " schedule(s) and task exported", "");
                    }
                } catch (Exception ex) {
                    LOGGER.error("failed to export schedule: query=" + query, ex);
                    if (tx != null && tx.isActive())
                        tx.rollback();
                    throw ex;
                }
            }
        } catch (Exception e) {
            LOGGER.error("exportToServer query=" + q, e);
            drawFacesError("Export failed: " + e.getMessage(), "");
        }
    }

    public void updateDueDate() {
        if (getSelectedRecord().getDueDate() != null) {
            Query q = queryController.getUpdateTaskDueDateQuery();
            try {
                if (q != null && getSelectedRecord().getIdExport() != null) {
                    SQLUtil sqlService = SQLUtil.createInstance();
                    final String[] queries = q.getQuery().split(";");
                    for (int i = 0; i < queries.length; i++) {

                        String query = queries[i]
                                .replaceAll(":task.idExport", getSelectedRecord().getIdExport().replaceAll("'", "''"))
                                .replaceAll(":task.dueDate", new SimpleDateFormat("yyyy-MM-dd").format(getSelectedRecord().getDueDate() != null ? getSelectedRecord().getDueDate() : new Date()).replaceAll("'", "''"));
                        sqlService.executeUpdate(query);	
					}
                }
                saveSelected();

                drawFacesInfoMessage("Duedate changed: " + new SimpleDateFormat("dd.MM.yyyy").format(getSelectedRecord().getDueDate()), "");

            } catch (Exception e) {
                LOGGER.error("Duedate change query=" + q, e);
                drawFacesError("Duedate change failed: " + e.getMessage(), "");
            }
        }
    }

    public boolean hasTaskUnexportedSchedules(String idExport) {
        for (Task t : getRecords()) {
            if (t.getIdExport() != null && t.getIdExport().equals(idExport)) {
                return hasTaskUnexportedSchedules((Task) t);
            }
        }
        return false;
    }

    private boolean hasTaskUnexportedSchedules(Task task) {
        if (task != null && task.getIdExport() != null) {
            scheduleController.initRelatedSchedules(task);
            for (Schedule s : scheduleController.getRelatedSchedules())
                if (!s.isExported())
                    return true;
        }
        return false;
    }

    public Date longToDate(Long l) {
        return new Date(l);
    }

    public Schedule getSchedule() {
        return schedule;
    }

    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }

    public int getSeconds() {
        return seconds;
    }

    public List<Map<String, List<Object>>> getColumnsBySQL() {
        if (columnsBySQL == null || columnsBySQL.isEmpty())
            initColumnsBySQL();
        return columnsBySQL;
    }

    public List<Object> getFilteredColumnsBySQL() {
        return filteredColumnsBySQL;
    }

    public void setFilteredColumnsBySQL(List<Object> filteredColumnsBySQL) {
        this.filteredColumnsBySQL = filteredColumnsBySQL;
    }

    public Map<String, ExportModel> getToExport() {
        return toExport;
    }

    public void setToExport(Map<String, ExportModel> toExport) {
        this.toExport = toExport;
    }
}
