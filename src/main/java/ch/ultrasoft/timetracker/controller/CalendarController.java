package ch.ultrasoft.timetracker.controller;

import java.math.BigInteger;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.TreeMap;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.timeline.TimelineSelectEvent;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.DefaultScheduleModel;
import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.ScheduleModel;
import org.primefaces.model.timeline.TimelineEvent;
import org.primefaces.model.timeline.TimelineModel;

import ch.ultrasoft.timetracker.model.AbstractEntity;
import ch.ultrasoft.timetracker.model.Task;
import ch.ultrasoft.timetracker.utils.SQLUtil;

@Named("calendarController")
@SessionScoped
public class CalendarController extends AbstractCrudController<AbstractEntity> {

	private static final long serialVersionUID = 7128810771691555739L;
	private static final Logger LOGGER = Logger.getLogger(CalendarController.class);
	private SQLUtil sqlService = null;
	private TimelineModel timelineModel; 
	private ScheduleModel scheduleModel;

	private TreeMap<String, ScheduleModel> scheduleModelMap = new TreeMap<>();
	private TreeMap<String, TimelineModel> timelineModelMap = new TreeMap<>();
	
	@Inject
	private QueryController queryController;
	@Inject
	private TaskController taskController;

	public CalendarController(Class<AbstractEntity> clazz) {
		super(clazz);
		try {
			sqlService = SQLUtil.createInstance();
		} catch (Exception e) {
			LOGGER.error("could not initialize SQLUtil", e);
		}
	}

	public CalendarController() {
		this(AbstractEntity.class);
	}

	public void initCalendars() {
		Integer queryIndex = 0;
		scheduleModelMap = new TreeMap<>();
		for (String q : queryController.getSelectTasksQuery().getQuery().replaceAll(":user.username", userBean.getUser().getUsername()).split(";")) {
			if (q != null && !q.trim().isEmpty()) {
				try {
					Map<String, List<Object>> map = sqlService.sqlToTasks(q);
					if (map.containsKey("data")) {
						String title = extractTitleFromMap(queryIndex, map);
						timelineModel = new TimelineModel();
						scheduleModel = new DefaultScheduleModel();
						for (Object o : map.get("data")) {
							taskController.setSelectedRecord((Map<String, Object>)o);
							Task t = taskController.getSelectedRecord();
							if (t.getDueDate() != null) {
								scheduleModel.addEvent(new DefaultScheduleEvent("#" + t.getIdExport() + " " + t.getTitle(), t.getDueDate(), t.getDueDate(), t));
								timelineModel.add(new TimelineEvent(t, cvtToGmt(t.getDueDate())));
							}
						}
						if (scheduleModel.getEventCount() > 0)
							scheduleModelMap.put(title, scheduleModel);
						if (timelineModel.getEvents() != null && timelineModel.getEvents().size() > 0)
							timelineModelMap.put(title, timelineModel);
					}
				} catch (SQLException e) {
					LOGGER.error("Could not initialise calendar", e);
					drawFacesError("Importing schedules failed: " + e.getMessage(), q);
				}
			}
			queryIndex++;
		}
	}

	private Date cvtToGmt(Date date) {
		TimeZone tz = TimeZone.getDefault();
		Date ret = new Date(date.getTime() - tz.getRawOffset());

		// if we are now in DST, back off by the delta. Note that we are
		// checking the GMT date, this is the KEY.
		if (tz.inDaylightTime(ret)) {
			Date dstDate = new Date(ret.getTime() - tz.getDSTSavings());

			// check to make sure we have not crossed back into standard time
			// this happens when we are on the cusp of DST (7pm the day before
			// the change for PDT)
			if (tz.inDaylightTime(dstDate)) {
				ret = dstDate;
			}
		}
		return ret;
	}
	
	private String extractTitleFromMap(Integer queryIndex, Map<String, List<Object>> map) {
		return map.containsKey("title") && map.get("title") != null && !map.get("title").isEmpty() 
					? (String) map.get("title").get(0) 
						: queryIndex.toString();
	}


	private List<ScheduleEvent> extractScheduleEventsFromMap(Map<String, List<Object>> map) {
		List<ScheduleEvent> events = new ArrayList<>();
		for (Object o : map.get("data")){
			TreeMap<String, Object> m = (TreeMap<String, Object>) o;
			if (m != null && m.containsKey("dueDate") && ((BigInteger) m.get("dueDate")).intValue() > 1 && m.containsKey("title") && m.containsKey("status"))
				events.add(new DefaultScheduleEvent((String) m.get("title") + " #" + (String) m.get("idExport"), new Date(((BigInteger)m.get("dueDate")).longValue()), null,(String) m.get("status")));
		}
		return events;
	}
	

	public void onScheduleEventSelect(SelectEvent e) {
		ScheduleEvent event = (ScheduleEvent) e.getObject();
		taskController.setSelectedRecord((Task)event.getData());
	}
	
	public void onTimelineSelect(TimelineSelectEvent e) {
		taskController.setSelectedRecord((Task)e.getTimelineEvent().getData());    
	}
	
	public TreeMap<String, ScheduleModel> getScheduleModelMap() {
		if (scheduleModelMap == null || scheduleModelMap.isEmpty())
			initCalendars();
		return scheduleModelMap;
	}

	public TreeMap<String, TimelineModel> getTimelineModelMap() {
		if (timelineModelMap == null || timelineModelMap.isEmpty())
			initCalendars();
		return timelineModelMap;
	}

	public TimelineModel getTimelineModel() {
		if (timelineModel == null)
			initCalendars();
		return timelineModel;
	}

	public void setTimelineModel(TimelineModel timelineModel) {
		this.timelineModel = timelineModel;
	}

	public ScheduleModel getScheduleModel() {
		return scheduleModel;
	}
}