package ch.ultrasoft.timetracker.controller;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;

import ch.ultrasoft.timetracker.utils.SQLUtil;

@Named("searchController")
@ViewScoped
public class SearchController implements Serializable {

	private static final long serialVersionUID = 7128810201691555739L;
	
	private static final Logger LOGGER = Logger.getLogger(SearchController.class);

	private SQLUtil sqlUtil = null;
	private String searchQuery = null;
	private Map<String, List<Object>> sqlToTasks = new TreeMap<>();
	
	@Inject
	private QueryController queryController;
	
	public void search() {
		String query = queryController.getSearchTaskQuery().getQuery();
		if (searchQuery != null && !searchQuery.isEmpty()) {
			searchQuery = searchQuery.replaceAll("'", "''");
			query = query.replaceAll(":search", searchQuery);
			try {
				if (getSqlUtil() != null) {
					sqlToTasks = getSqlUtil().sqlToTasks(query);
				}
			} catch (SQLException e) {
				LOGGER.error(String.format("Error executing SQLQuery:\n%s", query), e);
				FacesContext.getCurrentInstance().addMessage("", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error occured creating instance of SQLUtil", e.getMessage()));
			}
		}
	}
	
	private SQLUtil getSqlUtil() {
		if (sqlUtil == null)
			try {
				sqlUtil = SQLUtil.createInstance();
			} catch (Exception e) {
				LOGGER.error("Error occured creating instance of SQLUtil", e);
				sqlUtil = null;
				FacesContext.getCurrentInstance().addMessage("", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error occured creating instance of SQLUtil", e.getMessage()));
			}
		return sqlUtil;
	}

	public String getSearchQuery() {
		return searchQuery;
	}

	public void setSearchQuery(String searchQuery) {
		this.searchQuery = searchQuery;
	}

	public Map<String, List<Object>> getSqlToTasks() {
		return sqlToTasks;
	}

	public void setSqlToTasks(Map<String, List<Object>> sqlToTasks) {
		this.sqlToTasks = sqlToTasks;
	}
}