package ch.ultrasoft.timetracker.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

import ch.ultrasoft.timetracker.model.Schedule;
import ch.ultrasoft.timetracker.model.Task;

@Named("scheduleController")
@ViewScoped
public class ScheduleController extends AbstractCrudController<Schedule> {

	private static final long serialVersionUID = 7128810771691555798L;

	private TreeMap<String, Integer> dayTimeTrackerMap = new TreeMap<>();
	private List<Schedule> relatedSchedules = new ArrayList<Schedule>();

	public ScheduleController() {
		this(Schedule.class);
	}
	
	public ScheduleController(Class<Schedule> clazz) {
		super(clazz);
	}

	private void initDayTimeTracker() {
		dayTimeTrackerMap = new TreeMap<>();
		SimpleDateFormat isoShortDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		for (Schedule s : relatedSchedules){
			String key = isoShortDateFormat.format(s.getDate());
			if (s.isExported())
				key += " EXPORTED";
			if (dayTimeTrackerMap.containsKey(key)){
				dayTimeTrackerMap.put(key, dayTimeTrackerMap.get(key) + s.getTimeUsed());
			} else {
				dayTimeTrackerMap.put(key, s.getTimeUsed());
			}
		}
	}
	
	public void initRelatedSchedules(Task task) {
		relatedSchedules = new ArrayList<>();
		dayTimeTrackerMap.clear();
		if (task != null && task.getId() != null) {
			relatedSchedules = getService().getEm().createQuery("FROM Schedule s WHERE s.task.id = :idTask")
					.setParameter("idTask", task.getId())
					.getResultList();
			initDayTimeTracker();
		}
	}
	
	public void save(Schedule s) {
		setSelectedRecord(s);
		saveSelected();
	}
	
	public void delete(Schedule s) {
		setSelectedRecord(s);
		deleteSelected();
	}

	@Override
	public void deleteSelected() {
		Task tempTask = getSelectedRecord().getTask();
		super.deleteSelected();
		initRelatedSchedules(tempTask);
	}

	public List<Schedule> getRelatedSchedules() {
		return relatedSchedules;
	}

	public TreeMap<String, Integer> getDayTimeTrackerMap() {
		return dayTimeTrackerMap;
	}

	public void setDayTimeTrackerMap(TreeMap<String, Integer> dayTimeTrackerMap) {
		this.dayTimeTrackerMap = dayTimeTrackerMap;
	}
}
