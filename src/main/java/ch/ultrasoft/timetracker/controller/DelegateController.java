package ch.ultrasoft.timetracker.controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;

import ch.ultrasoft.timetracker.model.AbstractEntity;
import ch.ultrasoft.timetracker.utils.SQLUtil;

@Named("delegateController")
@SessionScoped
public class DelegateController extends AbstractCrudController<AbstractEntity>{

	private static final long serialVersionUID = 7128810771691555739L;
	private static final Logger LOGGER = Logger.getLogger(DelegateController.class);
	private SQLUtil sqlService = null;
	
	private List<String> idExportToDelegateList = new ArrayList<>();
	private List<String> usersToDelegateList = new ArrayList<>();
	private List<String> statusList = new ArrayList<>();
	
	private TreeSet<String> usernamesExt = new TreeSet<>();
	private String delegeteStatus = null;

	@Inject
	private QueryController queryController;
	
	@Inject
	private TaskController taskController;

	public DelegateController(Class<AbstractEntity> clazz) {
		super(clazz);
		try {
			sqlService = SQLUtil.createInstance();
		} catch (Exception e) {
			LOGGER.error("could not initialize SQLUtil", e);
		}
	}

	public DelegateController() {
		this(AbstractEntity.class);
	}

	@PostConstruct
	protected void importUsers() {
		ResultSet rs;
		try {
			SQLUtil sqlUtil = SQLUtil.createInstance();
			rs = sqlUtil.executeQuery(queryController.getSelectUsersQuery().getQuery());
			while (rs.next()) {
				usernamesExt.add(rs.getString("username"));
			}
			rs.close();
			sqlUtil.close();
		} catch (Exception e) {
			LOGGER.error("Import users failed", e);
		}
	}
	
	private void initStatusList() {
		statusList = new ArrayList<>();
		try {
			Map<String, List<Object>> map = sqlService.sqlToTasks(queryController.getSelectStatusQuery().getQuery());
			for (Object o : map.get("data")) {
				statusList.add(((Map<String, Object>)o).get("status").toString());
			}
		} catch (SQLException e) {
			LOGGER.error("failed to init Status", e);
			drawFacesError("failed to init Status: " + e.getMessage(), queryController.getSelectStatusQuery().getQuery());
		}
	}

	public void delegate() {
		boolean error = false;
		for (String idExport : idExportToDelegateList) {
			String users = "";
			for (String username : usersToDelegateList) {
				users += ",'" + username + "'";
			}
			
			String queryHistory = queryController.getHistoryTaskUserQuery().getQuery().replaceAll(":usernames", users.replaceAll("^,", "")).replaceAll(":idExport", idExport).replace(":user.username", userBean.getUser().getUsername());
			
			String query = queryController.getUpdateTaskUserQuery().getQuery().replaceAll(":usernames", users.replaceAll("^,", "")).replaceAll(":idExport", idExport).replace(":user.username", userBean.getUser().getUsername());
			try {
				sqlService.executeUpdate(queryHistory);
				sqlService.executeUpdate(query);
				
				if (delegeteStatus != null && !delegeteStatus.trim().isEmpty()){
					queryHistory = queryController.getHistoryTaskStatusQuery().getQuery().replaceAll(":idExport", idExport).replaceAll(":task.status", delegeteStatus).replace(":user.username", userBean.getUser().getUsername());
					query = queryController.getUpdateTaskStatusQuery().getQuery().replaceAll(":idExport", idExport).replaceAll(":task.status", delegeteStatus).replace(":user.username", userBean.getUser().getUsername());
					sqlService.executeUpdate(queryHistory);
					sqlService.executeUpdate(query);
				}
			} catch (Exception e) {
				error = true;
				drawFacesError("failed to delegate: " + e.getMessage(), query);
			}
		}
		idExportToDelegateList.clear();
		usersToDelegateList.clear();
		taskController.initColumnsBySQL();
		if (!error)
			drawFacesInfoMessage("Related successfully", "");
		delegeteStatus = null;
	}

	public boolean idExportToDelegateListContains(String idExport) {
		return idExportToDelegateList.contains(idExport);
	}

	public void addToDelegated(String idExport, String relatedUsers) {
		if (!idExportToDelegateListContains(idExport)) {
			usersToDelegateList.clear();
			usersToDelegateList.addAll(Arrays.asList(relatedUsers.split(",")));
			idExportToDelegateList.add(idExport);
			drawFacesInfoMessage("idExport added: #" + idExport, "");
		} else {
			drawFacesInfoMessage("idExport already added: #" + idExport, "");
		}
	}

	public void removeFromDelegated(String idExport) {
		idExportToDelegateList.remove(idExport);
		drawFacesInfoMessage("idExport removed: #" + idExport, "");
	}

	public List<String> getIdExportToDelegateList() {
		return idExportToDelegateList;
	}

	public void setIdExportToDelegateList(List<String> idExportToDelegateList) {
		this.idExportToDelegateList = idExportToDelegateList;
	}

	public TreeSet<String> getUsernamesExt() {
		return usernamesExt;
	}

	public void setUsernamesExt(TreeSet<String> usernamesExt) {
		this.usernamesExt = usernamesExt;
	}

	public List<String> getUsersToDelegateList() {
		if (usersToDelegateList == null || usersToDelegateList.isEmpty())
			importUsers();
		return usersToDelegateList;
	}
	
	public void setUsersToDelegateList(List<String> usersToDelegateList) {
		this.usersToDelegateList = usersToDelegateList;
	}
	
	public String getDelegeteStatus() {
		return delegeteStatus;
	}

	public void setDelegeteStatus(String delegeteStatus) {
		this.delegeteStatus = delegeteStatus;
	}

	public List<String> getStatusList() {
		if (statusList == null || statusList.isEmpty())
			initStatusList();
		return statusList;
	}
}