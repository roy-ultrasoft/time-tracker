package ch.ultrasoft.timetracker.controller;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.apache.log4j.Logger;

@Named("propertiesController")
@ViewScoped
public class PropertiesController implements Serializable {

	private static final long serialVersionUID = 7128810771691555739L;
	private static final Logger LOGGER = Logger.getLogger(PropertiesController.class);
	private static final String propertiesFileName = "src/main/resources/params.xml";
	private static final String[] propertiesKeys = new String[] { "url", "user", "password" };
	private static Map<String, String> propertiesMap = new HashMap<>();

	private static void initPropertiesMap() {
		Properties properties = new Properties();
		try {
			properties.loadFromXML(new FileInputStream(propertiesFileName));
			for (String s : propertiesKeys)
				propertiesMap.put(s, properties.getProperty(s));
			LOGGER.debug("properties imported");
		} catch (IOException e) {
			LOGGER.error("failed to initProperties", e);
			AbstractCrudController.drawFacesMessage(FacesContext.getCurrentInstance(), FacesMessage.SEVERITY_ERROR, "failed to import properties: " + e.getMessage(), "");
		}
	}

	public static void savePropertiesMap() {
		Properties properties = new Properties();
		try {
			for (String s : propertiesKeys)
				properties.put(s, propertiesMap.get(s));
			properties.storeToXML(new FileOutputStream(propertiesFileName), "", "utf-8");
			LOGGER.debug("properties saved");
			AbstractCrudController.drawFacesMessage(FacesContext.getCurrentInstance(), FacesMessage.SEVERITY_INFO, "properties saved: " + "changes will take effect after you restart", "");
		} catch (IOException e) {
			LOGGER.error("failed to save properties", e);
			AbstractCrudController.drawFacesMessage(FacesContext.getCurrentInstance(), FacesMessage.SEVERITY_ERROR, "failed to save properties: " + e.getMessage(), "");
		}
	}

	// needed for JSF
	public Map<String, String> getPropertiesMap() {
		return getStaticPropertiesMap();
	}

	// needed for JSF
	public void setPropertiesMap(Map<String, String> propertiesMap) {
		setStaticPropertiesMap(propertiesMap);
	}

	public static Map<String, String> getStaticPropertiesMap() {
		if (propertiesMap == null || propertiesMap.isEmpty())
			initPropertiesMap();
		return propertiesMap;
	}
	public static void setStaticPropertiesMap(Map<String, String> propertiesMap) {
		PropertiesController.propertiesMap = propertiesMap;
	}

}
