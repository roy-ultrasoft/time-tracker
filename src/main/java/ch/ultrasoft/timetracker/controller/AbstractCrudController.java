package ch.ultrasoft.timetracker.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.primefaces.event.CellEditEvent;
import org.primefaces.event.RowEditEvent;

import ch.ultrasoft.timetracker.beans.UserBean;
import ch.ultrasoft.timetracker.converter.AbstractEntityConverter;
import ch.ultrasoft.timetracker.model.AbstractEntity;
import ch.ultrasoft.timetracker.service.GenericService;

public abstract class AbstractCrudController<T extends AbstractEntity> implements Serializable {

	private static final long serialVersionUID = 1L;
	protected static final Logger LOGGER = Logger.getLogger(AbstractCrudController.class);
	
	@Inject
	protected UserBean userBean;
	
	private Class<T> clazz = null;

	private List<T> records = new ArrayList<>();
	private List<T> recordsFiltered = new ArrayList<>();
	private T selectedRecord;

	@Inject
	private GenericService<T> service;
	
	@Inject
	private AbstractEntityConverter<T> converter;

	public AbstractCrudController(Class<T> clazz) {
		this.clazz = clazz;
	}

	public void createRecord() {
		try {
			selectedRecord = getClazz().newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			LOGGER.error("could not create record", e);
		}
	}

	public void saveSelected() {
		if (selectedRecord == null)
			createRecord();
		setSelectedRecord((T) save(selectedRecord));
	}

	protected Object save(Object record) {
		LOGGER.debug("save: " + record);
		try {
			record = saveNoMessage(record);
			drawFacesInfoMessage("Record saved", record.toString());
		} catch (Exception e) {
			LOGGER.error("Failed to save record", e);
			drawFacesError("Failed to save record: " + e.getMessage(), record.toString());
		}
		initRecords();
		return record;
	}
	
	protected Object saveNoMessage(Object record) {
		LOGGER.debug("saveNoMessage: " + record);
		try {
			record = getService().save(selectedRecord);
		} catch (Exception e) {
			LOGGER.error("Failed to save record", e);
		}
		initRecords();
		return record;
	}

	protected void initRecords() {
		records = getService().getEm().createQuery("FROM " + getClazz().getSimpleName()).getResultList();
		recordsFiltered = records;
		try {
			if (selectedRecord == null)
				selectedRecord = (T) getClazz().newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			LOGGER.error("could not initialize Entyty: " + getClazz().getName(), e);
		}
	}

	public void onRowEdit(RowEditEvent event) {
		save((T) event.getObject());
	}

	public void onCellEdit(CellEditEvent event) {
		save((T) event.getNewValue());
	}

	public void deleteSelected() {
		try {
			getService().delete(selectedRecord);
			createRecord();
			drawFacesInfoMessage("Record deleted", selectedRecord.toString());
		} catch (Exception e) {
			LOGGER.error("failed to delete record", e);
			drawFacesError("failed to delete record: " + e.getMessage() , selectedRecord.toString());
		}
		initRecords();
	}

	protected void drawFacesInfoMessage(String sumary, String detail) {
		drawFacesMessage(FacesContext.getCurrentInstance(), FacesMessage.SEVERITY_INFO, sumary, detail);
	}

	protected void drawFacesWarnMessage(String sumary, String detail) {
		drawFacesMessage(FacesContext.getCurrentInstance(), FacesMessage.SEVERITY_WARN, sumary, detail);
	}

	protected void drawFacesError(String sumary, String detail) {
		drawFacesMessage(FacesContext.getCurrentInstance(), FacesMessage.SEVERITY_ERROR, sumary, detail);
	}

	public static void drawFacesMessage(FacesContext facescontext, Severity severityLevel, String sumary, String detail) {
		try {
			FacesMessage msg = new FacesMessage(severityLevel, sumary, detail);
			FacesContext.getCurrentInstance().addMessage(null, msg);
		} catch (Exception e) {
			LOGGER.warn("failed to draw message: " + e);
		}
	}
	
	private Class<T> getGenericType() {
		return clazz;
	}

	protected GenericService<T> getService(){
		return service;
	}

	public AbstractEntityConverter<T> getConverter() {
		return converter;
	}

	protected Class<T> getClazz() {
		if (clazz == null)
			clazz = getGenericType();
		return clazz;
	}

	public List<T> getRecords() {
		if (records == null || records.isEmpty())
			initRecords();
		return records;
	}

	public List<T> getRecordsFiltered() {
		return recordsFiltered;
	}

	public void setRecordsFiltered(List<T> recordsFiltered) {
		this.recordsFiltered = recordsFiltered;
	}

	public T getSelectedRecord() {
		return selectedRecord;
	}

	public void setSelectedRecord(T selectedRecord) {
		this.selectedRecord = selectedRecord;
	}
}