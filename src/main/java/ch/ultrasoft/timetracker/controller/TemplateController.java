package ch.ultrasoft.timetracker.controller;

import ch.ultrasoft.timetracker.model.Template;
import ch.ultrasoft.timetracker.model.User;
import org.apache.log4j.Logger;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.util.List;

@Named("templateController")
@SessionScoped
public class TemplateController extends AbstractCrudController<Template> {

	private static final long serialVersionUID = 7128810771691555889L;

	public TemplateController() {
		super(Template.class);
	}
}