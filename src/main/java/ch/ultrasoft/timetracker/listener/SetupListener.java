package ch.ultrasoft.timetracker.listener;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;

import javax.persistence.EntityManager;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import ch.ultrasoft.timetracker.model.Template;
import org.apache.log4j.Logger;

import ch.ultrasoft.timetracker.model.Query;
import ch.ultrasoft.timetracker.model.User;
import ch.ultrasoft.timetracker.persistence.EntityManagerProducer;
import ch.ultrasoft.timetracker.utils.SQLUtil;

/**
 * Application Lifecycle Listener implementation class SetupListener
 *
 */
@WebListener
public class SetupListener implements ServletContextListener {

	private static final Logger LOGGER = Logger.getLogger(SetupListener.class);
	private EntityManagerProducer emp = new EntityManagerProducer();
	private EntityManager em = emp.produceEntityManager();

	/**
	 * Default constructor.
	 */
	public SetupListener() {

	}

	/**
	 * @see ServletContextListener#contextDestroyed(ServletContextEvent)
	 */
	public void contextDestroyed(ServletContextEvent arg0) {
		System.out.println("\n" +
"         .-\"\"\"\"-._.'|\n" +
"        /     '.|   |\n" +
"       /        |   /        (\n" +
"      |      -= |  /|         )  (\n" +
"      |         |/`< )            )\n" +
"      ;       -=|  _|         ______\n" +
"       \\        \\  /        .'      `. /)\n" +
"        '._ _.-\"\"-.<      .'          `\\/)\n" +
"          / /      \\     / ___     .--'`/|\n" +
"       _ / |        '-._( ____\\   (____/_/=,\n" +
"      ( \\|  \\      -=/  /--;==============`\n" +
"    ._,;'\\==='-,..__/__/__.'\n" +
"   `'--/,/     || '  \\\n" +
"      / |  /   ||  '  \\\n" +
"      \\/  .    ||      ;\n" +
"      /   /    ||      |\n" +
"     |  .      ||      |\n" +
"     /         '=------|\n" +
"    / '    ;      ;   ;|\n" +
"   `-.___.___.___.___._/");
		
		try {
			SQLUtil sqlUtil = SQLUtil.createInstance();
			sqlUtil.close();
			LOGGER.info("SQLUtil closed");
			emp.closeEntityManagerFactory();
			LOGGER.info("EntityManagerFactory closed");
		} catch (Exception e) {
			LOGGER.error("failed to close SQLUtil");
		} 
	}

	/**
	 * @see ServletContextListener#contextInitialized(ServletContextEvent)
	 */
	public void contextInitialized(ServletContextEvent arg0) {
		
		System.out.println(
"									\n"+
"                       .========.\n" +
"                     .-|        |-.\n" +
"               .''-'`_.|__ __ __|._`'-''. \n" +
"              / .--'`  |[Gubbel]|  `'--. \\\n" +
"            .' /   _.--'''\"\"\"\"'''--._   \\ '.\n" +
"          /` .' .-'     _.----._     '-. '. `\\\n" +
"         |  /  /      .'  _  _  '.      \\  \\  |\n" +
"         | |   |     /   `_  _`   \\     |   | |\n" +
"        /  /   '.   |    (o)(o)    |   .'   \\  \\\n" +
"       |  |      '._|    .-\"\"-.    |_.'      |  |\n" +
"       |  \\       / |     \\  /     | \\       /  |\n" +
"       /  /      | / \\    /\\/\\    / \\ |      \\  \\\n" +
"       | |      / |   '-.(    ).-'   | \\      | |\n" +
"       | |     | /       \\`\"\"`/       \\ |     | |\n" +
"       \\  \\    / |    _.-|    |-._    | \\    /  /\n" +
"        \\  \\  | /   .'   |    |   '.  \\  |  /  /\n" +
"         '. './ | .'    /      \\    '. | \\.' .'\n" +
"           '._| \\/                    \\/ |_.'\n" +
"             `'{`   ,              ,   `}'`\n" +
"              {      }            {      }\n" +
"              {     }              {     }\n" +
"               {   }                {   }\n" +
"                \\,/                  \\,/\n" +
"                  '.                .'\n" +
"                    '-.__      __.-'\n" +
"                      {  _}\"\"{_  }\n" +
"                     /    \\  /    \\\n" +
"                    /=/=|=|  |=|=\\=\\\n" +
"                    \\/\\/\\_/  \\_/\\/\\/\n ");

		List<User> users = em.createQuery("FROM User").getResultList();

        Scanner scanner = new Scanner(System.in);
		if (users == null || users.isEmpty()) {
			System.out.println(
"\n                  +-----------------+\n"+ 
"                  |      SETUP      |\n"+ 
"                  +-----------------+\n");
			createUser(scanner);
			createQueries();
			createTemplates();
			try {
				configExportDB(scanner);
			} catch (IOException e) {
				LOGGER.error("failed to create properties", e);
			}
		} else {

			if (System.getProperties().containsKey("addUser")) {
				createUser(scanner);
			} if (System.getProperties().containsKey("setupDatabase")) {
				try {
					configExportDB(scanner);
				} catch (IOException e) {
					LOGGER.error("failed to create properties", e);
				}
			} if (System.getProperties().containsKey("createTemplates")) {
				createTemplates();
			} if (System.getProperties().containsKey("createQueries")) {
				createTemplates();
			}
		}
        scanner.close();
	}

	private void createUser(Scanner scanner) {
		User u = new User();
		System.out.print("Enter username (will be available in import and export queries): ");
		String username = scanner.nextLine();
		System.out.print("Enter password: ");
		String password = scanner.nextLine().trim();
		System.out.print("Enter kuerzel: ");
		String kuerzel = scanner.nextLine();
		System.out.print("Enter current sprint: ");
		String sprint = scanner.nextLine();
		u.setUsername(username);
		u.setKuerzel(kuerzel);
		u.setPass(password);
		u.setAktuellerSprint(sprint);
		try {
			em.getTransaction().begin();
			em.merge(u);
			em.flush();
			em.getTransaction().commit();
			LOGGER.debug("Created user: " + u.toString());
		} catch (Exception e) {
			LOGGER.error("failed to create user: " + u, e);
			if (em.getTransaction() != null && em.getTransaction().isActive())
				em.getTransaction().rollback();
		}
	}
	
	private void createQueries() {
		List<Query> queries = new ArrayList<>();

		LOGGER.info("Creating Queries");
		
		Query query = new Query();
		query.setTitle("EXPORT TASK");
		query.setTrigger(Query.Trigger.INSERT_TASK);
		query.setQuery(
						"INSERT INTO egw_timesheet (\n" + 
						"    ts_title, \n" + 
						"    ts_start, \n" + 
						"    ts_duration, \n" + 
						"    ts_quantity, \n" + 
						"    ts_owner, \n" + 
						"    ts_modified, \n" + 
						"    ts_modifier, cat_id) \n" + 
						"VALUES (\n" + 
						"    SUBSTRING(':task.title', 1, 80), \n" + 
						"    :schedule.dateAsLong / 1000, \n" + 
						"    :schedule.inHoursTimeUsed * 60, \n" + 
						"    :schedule.inHoursTimeUsed, \n" + 
						"    (    SELECT \n" + 
						"            account_id \n" + 
						"        FROM \n" + 
						"            egw_accounts \n" + 
						"        WHERE account_lid = ':user.username'\n" + 
						"    ), \n" + 
						"    :schedule.dateAsLong / 1000, \n" + 
						"    (SELECT account_id FROM egw_accounts WHERE account_lid = ':user.username'), \n" + 
						"    (SELECT CASE WHEN info_cat = 54 THEN 25 ELSE 24 END FROM egw_infolog WHERE info_id = :task.idExport))\n" + 
						"	 -- wenn infolog kategorie intern, dann wird status auf timesheet auf intern gesetzt sonst projektarbeiten");
		queries.add(query);

		query = new Query();
		query.setTitle("SELECT TASK");
		query.setTrigger(Query.Trigger.SELECT_TASK);
		query.setQuery(
						"-- Zu analysieren\n" + 
						"SELECT \n" + 
						"	egw_infolog.info_id AS idExport, \n" + 
						"	egw_infolog.info_subject AS title, \n" + 
						"	egw_infolog.info_status AS status,\n" + 
						"	CASE WHEN egw_infolog_extra.info_extra_value IS NULL THEN\n" + 
						"		egw_infolog.info_enddate * 1000\n" + 
						"	ELSE\n" + 
						"		UNIX_TIMESTAMP(STR_TO_DATE(egw_infolog_extra.info_extra_value, '%Y-%m-%d')) * 1000\n" + 
						"	END AS dueDate,\n" + 
						"	egw_infolog.info_des as description,\n" +
						"  (SELECT GROUP_CONCAT(DISTINCT egw_accounts.account_lid) FROM egw_accounts WHERE (FIND_IN_SET(egw_accounts.account_id,\n" +
						"\t  (SELECT nested_infolog.info_responsible FROM  egw_infolog as nested_infolog WHERE nested_infolog.info_id = egw_infolog.info_id LIMIT 1)\n" +
						"\t)) > 0) AS relatedUsers" +
						"FROM \n" + 
						"	egw_infolog \n" + 
						"	LEFT OUTER JOIN egw_infolog_extra ON\n" + 
						"		egw_infolog.info_id = egw_infolog_extra.info_id AND egw_infolog_extra.info_extra_name = 'interne_abgabe'\n" + 
						"WHERE \n" + 
						"	(info_responsible LIKE CONCAT('%', (SELECT account_id FROM egw_accounts WHERE account_lid = ':user.username'), '%')\n" + 
						"	OR (info_responsible LIKE '0' AND  info_owner = (SELECT account_id FROM egw_accounts WHERE account_lid = ':user.username')))\n" + 
						"	AND info_status LIKE '%analyse%'\n" + 
						"ORDER BY info_datemodified DESC;\n" + 
						"-- zu Programmieren\n" + 
						"SELECT \n" + 
						"	egw_infolog.info_id AS idExport, \n" + 
						"	egw_infolog.info_subject AS title, \n" + 
						"	egw_infolog.info_status AS status,\n" + 
						"	CASE WHEN egw_infolog_extra.info_extra_value IS NULL THEN\n" + 
						"		egw_infolog.info_enddate * 1000\n" + 
						"	ELSE\n" + 
						"		UNIX_TIMESTAMP(STR_TO_DATE(egw_infolog_extra.info_extra_value, '%Y-%m-%d')) * 1000\n" + 
						"	END AS dueDate,\n" + 
						"	egw_infolog.info_des as description,\n" +
						"  (SELECT GROUP_CONCAT(DISTINCT egw_accounts.account_lid) FROM egw_accounts WHERE (FIND_IN_SET(egw_accounts.account_id,\n" +
						"\t  (SELECT nested_infolog.info_responsible FROM  egw_infolog as nested_infolog WHERE nested_infolog.info_id = egw_infolog.info_id LIMIT 1)\n" +
						"\t)) > 0) AS relatedUsers" +
						"FROM \n" + 
						"	egw_infolog \n" + 
						"	LEFT OUTER JOIN egw_infolog_extra ON\n" + 
						"		egw_infolog.info_id = egw_infolog_extra.info_id AND egw_infolog_extra.info_extra_name = 'interne_abgabe'\n" + 
						"WHERE \n" + 
						"	(info_responsible LIKE CONCAT('%', (SELECT account_id FROM egw_accounts WHERE account_lid = ':user.username'), '%')\n" + 
						"	OR (info_responsible LIKE '0' AND  info_owner = (SELECT account_id FROM egw_accounts WHERE account_lid = ':user.username')))\n" + 
						"	AND info_status IN ('programmieren', 'korrekturen')\n" + 
						"ORDER BY info_datemodified DESC;\n" + 
						"-- Zu testen\n" + 
						"SELECT \n" + 
						"	egw_infolog.info_id AS idExport, \n" + 
						"	egw_infolog.info_subject AS title, \n" + 
						"	egw_infolog.info_status AS status,\n" + 
						"	CASE WHEN egw_infolog_extra.info_extra_value IS NULL THEN\n" + 
						"		egw_infolog.info_enddate * 1000\n" + 
						"	ELSE\n" + 
						"		UNIX_TIMESTAMP(STR_TO_DATE(egw_infolog_extra.info_extra_value, '%Y-%m-%d')) * 1000\n" + 
						"	END AS dueDate,\n" + 
						"	egw_infolog.info_des as description,\n" +
						"  (SELECT GROUP_CONCAT(DISTINCT egw_accounts.account_lid) FROM egw_accounts WHERE (FIND_IN_SET(egw_accounts.account_id,\n" +
						"\t  (SELECT nested_infolog.info_responsible FROM  egw_infolog as nested_infolog WHERE nested_infolog.info_id = egw_infolog.info_id LIMIT 1)\n" +
						"\t)) > 0) AS relatedUsers" +
						"FROM \n" + 
						"	egw_infolog \n" + 
						"	LEFT OUTER JOIN egw_infolog_extra ON\n" + 
						"		egw_infolog.info_id = egw_infolog_extra.info_id AND egw_infolog_extra.info_extra_name = 'interne_abgabe'\n" + 
						"WHERE \n" + 
						"	(info_responsible LIKE CONCAT('%', (SELECT account_id FROM egw_accounts WHERE account_lid = ':user.username'), '%')\n" + 
						"	OR (info_responsible LIKE '0' AND  info_owner = (SELECT account_id FROM egw_accounts WHERE account_lid = ':user.username')))\n" + 
						"	AND info_status LIKE '%test%'\n" + 
						"ORDER BY info_datemodified DESC;\n" + 
						"-- Zu installieren\n" + 
						"SELECT \n" + 
						"	egw_infolog.info_id AS idExport, \n" + 
						"	egw_infolog.info_subject AS title, \n" + 
						"	egw_infolog.info_status AS status,\n" + 
						"	CASE WHEN egw_infolog_extra.info_extra_value IS NULL THEN\n" + 
						"		egw_infolog.info_enddate * 1000\n" + 
						"	ELSE\n" + 
						"		UNIX_TIMESTAMP(STR_TO_DATE(egw_infolog_extra.info_extra_value, '%Y-%m-%d')) * 1000\n" + 
						"	END AS dueDate,\n" + 
						"	egw_infolog.info_des as description,\n" +
						"  (SELECT GROUP_CONCAT(DISTINCT egw_accounts.account_lid) FROM egw_accounts WHERE (FIND_IN_SET(egw_accounts.account_id,\n" +
						"\t  (SELECT nested_infolog.info_responsible FROM  egw_infolog as nested_infolog WHERE nested_infolog.info_id = egw_infolog.info_id LIMIT 1)\n" +
						"\t)) > 0) AS relatedUsers" +
						"FROM \n" + 
						"	egw_infolog \n" + 
						"	LEFT OUTER JOIN egw_infolog_extra ON\n" + 
						"		egw_infolog.info_id = egw_infolog_extra.info_id AND egw_infolog_extra.info_extra_name = 'interne_abgabe'\n" + 
						"WHERE \n" + 
						"	(info_responsible LIKE CONCAT('%', (SELECT account_id FROM egw_accounts WHERE account_lid = ':user.username'), '%')\n" + 
						"	OR (info_responsible LIKE '0' AND  info_owner = (SELECT account_id FROM egw_accounts WHERE account_lid = ':user.username')))\n" + 
						"	AND info_status IN ('installation')\n" + 
						"ORDER BY info_datemodified DESC;\n" + 
						"-- Installiert Entwicklung\n" + 
						"SELECT \n" + 
						"	egw_infolog.info_id AS idExport, \n" + 
						"	egw_infolog.info_subject AS title, \n" + 
						"	egw_infolog.info_status AS status,\n" + 
						"	CASE WHEN egw_infolog_extra.info_extra_value IS NULL THEN\n" + 
						"		egw_infolog.info_enddate * 1000\n" + 
						"	ELSE\n" + 
						"		UNIX_TIMESTAMP(STR_TO_DATE(egw_infolog_extra.info_extra_value, '%Y-%m-%d')) * 1000\n" + 
						"	END AS dueDate,\n" + 
						"	egw_infolog.info_des as description,\n" +
						"  (SELECT GROUP_CONCAT(DISTINCT egw_accounts.account_lid) FROM egw_accounts WHERE (FIND_IN_SET(egw_accounts.account_id,\n" +
						"\t  (SELECT nested_infolog.info_responsible FROM  egw_infolog as nested_infolog WHERE nested_infolog.info_id = egw_infolog.info_id LIMIT 1)\n" +
						"\t)) > 0) AS relatedUsers" +
						"FROM \n" + 
						"	egw_infolog \n" + 
						"	LEFT OUTER JOIN egw_infolog_extra ON\n" + 
						"		egw_infolog.info_id = egw_infolog_extra.info_id AND egw_infolog_extra.info_extra_name = 'interne_abgabe'\n" + 
						"WHERE \n" + 
						"	(info_responsible LIKE CONCAT('%', (SELECT account_id FROM egw_accounts WHERE account_lid = ':user.username'), '%')\n" + 
						"	OR (info_responsible LIKE '0' AND  info_owner = (SELECT account_id FROM egw_accounts WHERE account_lid = ':user.username')))\n" + 
						"	AND info_status IN ('instDev')\n" + 
						"ORDER BY info_datemodified DESC;\n" + 
						"-- Installiert Schulung\n" + 
						"SELECT \n" + 
						"	egw_infolog.info_id AS idExport, \n" + 
						"	egw_infolog.info_subject AS title, \n" + 
						"	egw_infolog.info_status AS status,\n" + 
						"	CASE WHEN egw_infolog_extra.info_extra_value IS NULL THEN\n" + 
						"		egw_infolog.info_enddate * 1000\n" + 
						"	ELSE\n" + 
						"		UNIX_TIMESTAMP(STR_TO_DATE(egw_infolog_extra.info_extra_value, '%Y-%m-%d')) * 1000\n" + 
						"	END AS dueDate,\n" + 
						"	egw_infolog.info_des as description,\n" +
						"  (SELECT GROUP_CONCAT(DISTINCT egw_accounts.account_lid) FROM egw_accounts WHERE (FIND_IN_SET(egw_accounts.account_id,\n" +
						"\t  (SELECT nested_infolog.info_responsible FROM  egw_infolog as nested_infolog WHERE nested_infolog.info_id = egw_infolog.info_id LIMIT 1)\n" +
						"\t)) > 0) AS relatedUsers" +
						"FROM \n" + 
						"	egw_infolog \n" + 
						"	LEFT OUTER JOIN egw_infolog_extra ON\n" + 
						"		egw_infolog.info_id = egw_infolog_extra.info_id AND egw_infolog_extra.info_extra_name = 'interne_abgabe'\n" + 
						"WHERE \n" + 
						"	(info_responsible LIKE CONCAT('%', (SELECT account_id FROM egw_accounts WHERE account_lid = ':user.username'), '%')\n" + 
						"	OR (info_responsible LIKE '0' AND  info_owner = (SELECT account_id FROM egw_accounts WHERE account_lid = ':user.username')))\n" + 
						"	AND info_status IN ('instSchul')\n" + 
						"ORDER BY info_datemodified DESC;\n" + 
						"-- Warten\n" + 
						"SELECT \n" + 
						"	egw_infolog.info_id AS idExport, \n" + 
						"	egw_infolog.info_subject AS title, \n" + 
						"	egw_infolog.info_status AS status,\n" + 
						"	CASE WHEN egw_infolog_extra.info_extra_value IS NULL THEN\n" + 
						"		egw_infolog.info_enddate * 1000\n" + 
						"	ELSE\n" + 
						"		UNIX_TIMESTAMP(STR_TO_DATE(egw_infolog_extra.info_extra_value, '%Y-%m-%d')) * 1000\n" + 
						"	END AS dueDate,\n" + 
						"	egw_infolog.info_des as description,\n" +
						"  (SELECT GROUP_CONCAT(DISTINCT egw_accounts.account_lid) FROM egw_accounts WHERE (FIND_IN_SET(egw_accounts.account_id,\n" +
						"\t  (SELECT nested_infolog.info_responsible FROM  egw_infolog as nested_infolog WHERE nested_infolog.info_id = egw_infolog.info_id LIMIT 1)\n" +
						"\t)) > 0) AS relatedUsers" +
						"FROM \n" + 
						"	egw_infolog \n" + 
						"	LEFT OUTER JOIN egw_infolog_extra ON\n" + 
						"		egw_infolog.info_id = egw_infolog_extra.info_id AND egw_infolog_extra.info_extra_name = 'interne_abgabe'\n" + 
						"WHERE \n" + 
						"	(info_responsible LIKE CONCAT('%', (SELECT account_id FROM egw_accounts WHERE account_lid = ':user.username'), '%')\n" + 
						"	OR (info_responsible LIKE '0' AND  info_owner = (SELECT account_id FROM egw_accounts WHERE account_lid = ':user.username')))\n" + 
						"	AND info_status LIKE '%warten%'\n" + 
						"ORDER BY info_datemodified DESC;\n" + 
						"-- Andere\n" + 
						"SELECT \n" + 
						"	egw_infolog.info_id AS idExport, \n" + 
						"	egw_infolog.info_subject AS title, \n" + 
						"	egw_infolog.info_status AS status,\n" + 
						"	CASE WHEN egw_infolog_extra.info_extra_value IS NULL THEN\n" + 
						"		egw_infolog.info_enddate * 1000\n" + 
						"	ELSE\n" + 
						"		UNIX_TIMESTAMP(STR_TO_DATE(egw_infolog_extra.info_extra_value, '%Y-%m-%d')) * 1000\n" + 
						"	END AS dueDate,\n" + 
						"	egw_infolog.info_des as description,\n" +
						"  (SELECT GROUP_CONCAT(DISTINCT egw_accounts.account_lid) FROM egw_accounts WHERE (FIND_IN_SET(egw_accounts.account_id,\n" +
						"\t  (SELECT nested_infolog.info_responsible FROM  egw_infolog as nested_infolog WHERE nested_infolog.info_id = egw_infolog.info_id LIMIT 1)\n" +
						"\t)) > 0) AS relatedUsers" +
						"FROM \n" + 
						"	egw_infolog \n" + 
						"	LEFT OUTER JOIN egw_infolog_extra ON\n" + 
						"		egw_infolog.info_id = egw_infolog_extra.info_id AND egw_infolog_extra.info_extra_name = 'interne_abgabe'\n" + 
						"WHERE \n" + 
						"	(info_responsible LIKE CONCAT('%', (SELECT account_id FROM egw_accounts WHERE account_lid = ':user.username'), '%') \n" + 
						"	OR (info_responsible LIKE '0' AND  info_owner = (SELECT account_id FROM egw_accounts WHERE account_lid = ':user.username')))\n" + 
						"	AND info_status NOT IN ('done', 'deleted','billed', 'cancelled', 'programmieren', 'installation', 'instDev', 'instSchul', 'korrekturen', 'warten','test1','test2','analyse')\n" + 
						"ORDER BY info_datemodified DESC;\n" + 
						"-- Sprint (to code) Insassenverwaltung\n" + 
						"SELECT DISTINCT\n" + 
						"	egw_infolog.info_id AS idExport, \n" + 
						"	egw_infolog.info_subject AS title, \n" + 
						"	egw_infolog.info_status AS status,\n" + 
						"	CASE WHEN egw_infolog_extra.info_extra_value IS NULL THEN\n" + 
						"		egw_infolog.info_enddate * 1000\n" + 
						"	ELSE\n" + 
						"		UNIX_TIMESTAMP(STR_TO_DATE(egw_infolog_extra.info_extra_value, '%Y-%m-%d')) * 1000\n" + 
						"	END AS dueDate,\n" + 
						"	egw_infolog.info_des as description,\n" +
						"  (SELECT GROUP_CONCAT(DISTINCT egw_accounts.account_lid) FROM egw_accounts WHERE (FIND_IN_SET(egw_accounts.account_id,\n" +
						"\t  (SELECT nested_infolog.info_responsible FROM  egw_infolog as nested_infolog WHERE nested_infolog.info_id = egw_infolog.info_id LIMIT 1)\n" +
						"\t)) > 0) AS relatedUsers" +
						"FROM egw_infolog\n" + 
						"	INNER JOIN egw_links ON\n" + 
						"	        egw_links.link_id1 = egw_infolog.info_id\n" + 
						"        LEFT OUTER JOIN egw_infolog_extra ON\n" + 
						"		egw_infolog.info_id = egw_infolog_extra.info_id AND egw_infolog_extra.info_extra_name = 'interne_abgabe'	\n" + 
						"WHERE link_id2 IN (SELECT pm_id FROM egw_pm_projects WHERE pm_title = ':user.sprint') \n" +
						"AND info_status NOT IN ('done', 'deleted','billed', 'cancelled', 'verrechnen')\n" + 
						"AND info_status NOT LIKE '%test%'\n" + 
						"ORDER BY info_datemodified DESC;\n" + 
						"-- Sprint (to test) Insassenverwaltung\n" + 
						"SELECT DISTINCT\n" + 
						"	egw_infolog.info_id AS idExport, \n" + 
						"	egw_infolog.info_subject AS title, \n" + 
						"	egw_infolog.info_status AS status,\n" + 
						"	CASE WHEN egw_infolog_extra.info_extra_value IS NULL THEN\n" + 
						"		egw_infolog.info_enddate * 1000\n" + 
						"	ELSE\n" + 
						"		UNIX_TIMESTAMP(STR_TO_DATE(egw_infolog_extra.info_extra_value, '%Y-%m-%d')) * 1000\n" + 
						"	END AS dueDate,\n" + 
						"	egw_infolog.info_des as description,\n" +
						"  (SELECT GROUP_CONCAT(DISTINCT egw_accounts.account_lid) FROM egw_accounts WHERE (FIND_IN_SET(egw_accounts.account_id,\n" +
						"\t  (SELECT nested_infolog.info_responsible FROM  egw_infolog as nested_infolog WHERE nested_infolog.info_id = egw_infolog.info_id LIMIT 1)\n" +
						"\t)) > 0) AS relatedUsers" +
						"FROM egw_infolog\n" + 
						"	INNER JOIN egw_links ON\n" + 
						"	        egw_links.link_id1 = egw_infolog.info_id\n" + 
						"        LEFT OUTER JOIN egw_infolog_extra ON\n" + 
						"		egw_infolog.info_id = egw_infolog_extra.info_id AND egw_infolog_extra.info_extra_name = 'interne_abgabe'		\n" + 
						"WHERE link_id2 IN (SELECT pm_id FROM egw_pm_projects WHERE pm_title = ':user.sprint')\n" +
						"AND info_status LIKE '%test%'\n" + 
						"ORDER BY info_datemodified DESC;\n" + 
						"-- Support\n" + 
						"SELECT \n" + 
						"	egw_infolog.info_id AS idExport, \n" + 
						"	egw_infolog.info_subject AS title, \n" + 
						"	egw_infolog.info_status AS status,\n" + 
						"	CASE WHEN egw_infolog_extra.info_extra_value IS NULL THEN\n" + 
						"		egw_infolog.info_enddate * 1000\n" + 
						"	ELSE\n" + 
						"		UNIX_TIMESTAMP(STR_TO_DATE(egw_infolog_extra.info_extra_value, '%Y-%m-%d')) * 1000\n" + 
						"	END AS dueDate,\n" + 
						"	egw_infolog.info_des as description,\n" +
						"  (SELECT GROUP_CONCAT(DISTINCT egw_accounts.account_lid) FROM egw_accounts WHERE (FIND_IN_SET(egw_accounts.account_id,\n" +
						"\t  (SELECT nested_infolog.info_responsible FROM  egw_infolog as nested_infolog WHERE nested_infolog.info_id = egw_infolog.info_id LIMIT 1)\n" +
						"\t)) > 0) AS relatedUsers" +
						"FROM \n" + 
						"	egw_infolog \n" + 
						"	LEFT OUTER JOIN egw_infolog_extra ON\n" + 
						"		egw_infolog.info_id = egw_infolog_extra.info_id AND egw_infolog_extra.info_extra_name = 'interne_abgabe'\n" + 
						"WHERE \n" + 
						"	(info_responsible LIKE CONCAT('%', (SELECT account_id FROM egw_accounts WHERE account_lid = 'gina_support'), '%') \n" + 
						"	OR (info_responsible LIKE '0' AND  info_owner = (SELECT account_id FROM egw_accounts WHERE account_lid = 'gina_support')))\n" + 
						"	AND info_status NOT IN ('done', 'deleted','billed', 'cancelled')\n" + 
						"ORDER BY info_datemodified DESC;");
		queries.add(query);
		
		query = new Query();
		query.setTitle("EXPORT TASK");
		query.setTrigger(Query.Trigger.INSERT_TASK_SHEDULE_RELATION);
		query.setQuery(
						"INSERT INTO egw_links (\n" + 
						"    link_app1,\n" + 
						"    link_id1,\n" + 
						"    link_app2,\n" + 
						"    link_id2,\n" + 
						"    link_lastmod,\n" + 
						"    link_owner) \n" + 
						"VALUES (\n" + 
						"    'timesheet',\n" + 
						"    :insered.schedule.id,\n" + 
						"    'infolog',\n" + 
						"    :task.idExport,\n" + 
						"    :schedule.dateAsLong / 1000, \n" + 
						"    (    SELECT \n" + 
						"             account_id \n" + 
						"         FROM \n" + 
						"             egw_accounts \n" + 
						"         WHERE account_lid = ':user.username')\n" + 
						"); \n" + 
						"UPDATE egw_infolog \n" + 
						"    SET info_des = (SELECT CONCAT(':schedule.description.summary', '\n" + 
						"', info_des)),\n" + 
						"    info_modifier = (    SELECT \n" + 
						"             account_id \n" + 
						"         FROM \n" + 
						"             egw_accounts \n" + 
						"         WHERE account_lid = ':user.username'),\n" + 
						"info_datemodified = (SELECT UNIX_TIMESTAMP(NOW()))\n" + 
						"WHERE info_id = :task.idExport");
		queries.add(query);

		query = new Query();
		query.setTitle("USERS");
		query.setTrigger(Query.Trigger.SELECT_USER);
		query.setQuery(
						"select \n" + 
						"    account_id as idExport, \n" + 
						"    account_lid as username \n" + 
						"from \n" + 
						"    egw_accounts \n" + 
						"WHERE \n" + 
						"    account_lid in ('gina_planung', 'gina_support', 'hotel_support', 'bit', 'cerny', 'cyrill', 'flurina', 'hal', 'lam', 'mul', 'ryan', 'roy', 'schj', 'vot') \n" + 
						"ORDER BY username");
		queries.add(query);

		query = new Query();
		query.setTitle("DELEGATE TASKS");
		query.setTrigger(Query.Trigger.UPDATE_TASK_USER);
		query.setQuery(
						"UPDATE  egw_infolog \n" + 
						"    set info_responsible = (SELECT GROUP_CONCAT(account_id) from egw_accounts WHERE account_lid in (:usernames)),\n" + 
						"    info_modifier = (    SELECT \n" + 
						"             account_id \n" + 
						"         FROM \n" + 
						"             egw_accounts \n" + 
						"         WHERE account_lid = ':user.username'),\n" + 
						"info_datemodified = (SELECT UNIX_TIMESTAMP(NOW()))\n" + 
						"WHERE info_id = :idExport");
		queries.add(query);

		query = new Query();
		query.setTitle("UPDATE_TASK_STATUS");
		query.setTrigger(Query.Trigger.UPDATE_TASK_STATUS);
		query.setQuery(
						"UPDATE egw_infolog \n" + 
						"    set info_status = ':task.status', info_type = 'task' \n" + 
						"WHERE info_id = :idExport");
		queries.add(query);
		
		query = new Query();
		query.setTitle("SELECT_STATUS");
		query.setTrigger(Query.Trigger.SELECT_STATUS);
		query.setQuery(
						"SELECT 'analyse' AS status \n" + 
						"UNION SELECT 'installation' AS status \n" + 
						"UNION SELECT 'instDev' AS status \n" + 
						"UNION SELECT 'instSchul' AS status \n" + 
						"UNION SELECT 'kunde' AS status \n" + 
						"UNION SELECT 'programmieren' AS status \n" + 
						"UNION SELECT 'test1' AS status \n" + 
						"UNION SELECT 'test2' AS status \n" + 
						"UNION SELECT 'verrechnen' AS status \n" + 
						"UNION SELECT 'warten' AS status");
		queries.add(query);
		

		query = new Query();
		query.setTitle("SEARCH_TASK");
		query.setTrigger(Query.Trigger.SEARCH_TASK);
		query.setQuery(	"-- Suche \n" + 
				"-- Suche \n" + 
				"SELECT \n" + 
				"	info_id AS idExport, \n" + 
				"	info_subject AS title, \n" + 
				"	info_status AS status,\n" + 
				"	CAST(info_datemodified * 1000 AS UNSIGNED) AS dueDate,\n" + 
				"	info_des as description\n" + 
				"FROM \n" + 
				"	egw_infolog \n" + 
				"WHERE \n" + 
				"	info_id LIKE '%:search%'\n" + 
				"	OR info_subject LIKE '%:search%'\n" + 
				"	OR info_status LIKE '%:search%'\n" + 
				"	OR info_des LIKE '%:search%'\n" + 
				"ORDER BY info_datemodified DESC;");
		queries.add(query);
		
		query = new Query();
		query.setTitle("HISTORY_TASK");
		query.setTrigger(Query.Trigger.HISTORY_TASK);
		query.setQuery(	"-- HISTORY_TASK \n" + 
				"INSERT INTO egw_history_log (\n" + 
				"	history_record_id,\n" + 
				"	history_appname,\n" + 
				"	history_owner,\n" + 
				"	history_status,\n" + 
				"	history_new_value,\n" + 
				"	history_timestamp,\n" + 
				"	history_old_value) \n" + 
				"VALUES (\n" + 
				"	:task.idExport,\n" + 
				"	'infolog',\n" + 
				"	(SELECT account_id FROM egw_accounts WHERE  account_lid = ':user.username'),\n" + 
				"	'De',\n" + 
				"	CONCAT(':schedule.description.summary', '\\n', (SELECT info_des FROM egw_infolog WHERE info_id = :task.idExport)),\n" + 
				"	NOW(),\n" + 
				"	(SELECT info_des FROM egw_infolog WHERE info_id = :task.idExport)\n" + 
				");");
		queries.add(query);
		
		query = new Query();
		query.setTitle("HISTORY_TASK_STATUS");
		query.setTrigger(Query.Trigger.HISTORY_TASK_STATUS);
		query.setQuery(	"-- HISTORY_TASK_STATUS \n" + 
				"INSERT INTO egw_history_log (\n" + 
				"	history_record_id,\n" + 
				"	history_appname,\n" + 
				"	history_owner,\n" + 
				"	history_status,\n" + 
				"	history_new_value,\n" + 
				"	history_timestamp,\n" + 
				"	history_old_value) \n" + 
				"VALUES (\n" + 
				"	:idExport,\n" + 
				"	'infolog',\n" + 
				"	(SELECT account_id from egw_accounts WHERE account_lid = ':user.username'),\n" + 
				"	'St',\n" + 
				"	':task.status',\n" + 
				"	NOW(),\n" + 
				"	(SELECT info_status FROM egw_infolog h WHERE h.info_id = 38079));");
		queries.add(query);

		query = new Query();
		query.setTitle("HISTORY_TASK_USER");
		query.setTrigger(Query.Trigger.HISTORY_TASK_USER);
		query.setQuery(	"-- HISTORY_TASK_USER \n" +
				"INSERT INTO egw_history_log (\n" +
				"	history_record_id,\n" +
				"	history_appname,\n" +
				"	history_owner,\n" +
				"	history_status,\n" +
				"	history_new_value,\n" +
				"	history_timestamp,\n" +
				"	history_old_value\n" +
				") VALUES (\n" +
				"	:idExport,\n" +
				"	'infolog',\n" +
				"	(SELECT account_id from egw_accounts WHERE account_lid = ':user.username'),\n" +
				"	'Re',\n" +
				"	(SELECT GROUP_CONCAT(account_id) from egw_accounts WHERE account_lid in (:usernames)),\n" +
				"	NOW(),\n" +
				"	(SELECT info_responsible FROM egw_infolog WHERE info_id = :idExport));");
		queries.add(query);

		query = new Query();
		query.setTitle("UPDATE_TASK_DUEDATE");
		query.setTrigger(Query.Trigger.UPDATE_TASK_DUEDATE);
		query.setQuery(	"-- UPDATE_TASK_DUEDATE \n" +
				"-- UPDATE_TASK_DUEDATE \n" + 
				"UPDATE egw_infolog_extra SET egw_infolog_extra.info_extra_value = ':task.dueDate' WHERE egw_infolog_extra.info_id = :task.idExport AND egw_infolog_extra.info_extra_name = 'interne_abgabe';\n" + 
				"UPDATE egw_infolog_extra SET egw_infolog_extra.info_extra_value = ':task.dueDate' WHERE egw_infolog_extra.info_id = :task.idExport AND egw_infolog_extra.info_extra_name = 'externe_abgabe';\n" + 
				"UPDATE egw_infolog SET egw_infolog.info_enddate = (UNIX_TIMESTAMP(STR_TO_DATE(':task.dueDate', '%Y-%m-%d')) / 1000) WHERE egw_infolog.info_id = :task.idExport");
		queries.add(query);
		
		try {
			em.getTransaction().begin();
			for (Query q : queries)
				em.merge(q);
			em.flush();
			em.getTransaction().commit();
			LOGGER.info("Queries created");
		} catch (Exception e) {
			LOGGER.error("failed to create queries: ", e);
			if (em.getTransaction() != null && em.getTransaction().isActive())
				em.getTransaction().rollback();
		}
	}

	private void createTemplates() {
		List<Template> templates = new ArrayList<>();

		LOGGER.info("Creating Templates");

		Template template = new Template();
		template.setTitle("Mail an Kunde - DE");
		template.setContent("Guten Tag\n" +
				"\n" +
				"Betreffend Ihrer Anfrage vom 01.01.2019:\n" +
				"\n" +
				"    => Bla Bla Bal\n" +
				"\n" +
				"Haben wir folgende Fragen:\n" +
				"\n" +
				"    - Woher ?\n" +
				"\n" +
				"Können wir Ihnen folgende Antwort geben:\n" +
				"\n" +
				"    - Deshalb ...\n" +
				"\n" +
				"Bei Fragen oder Unklarheiten stehen wir ihnen zur Verfügung.");
		templates.add(template);

		template = new Template();
		template.setTitle("Mail an Kunde - FR");
		template.setContent("Bonjour\n" +
				"\n" +
				"Concernant votre demande  du 01.01.2019 :\n" +
				"\n" +
				"Blah Blah Blah Bal => Blah Blah Bal\n" +
				"\n" +
				"Nous avons les questions suivantes :\n" +
				"\n" +
				"    - D'où viennent-ils ?\n" +
				"\n" +
				"Pouvons-nous vous donner la réponse suivante ?\n" +
				"\n" +
				"    - Par conséquent, ....\n" +
				"\n" +
				"En cas de questions ou d'ambiguïtés, nous sommes à votre disposition.");
		templates.add(template);

		template = new Template();
		template.setTitle("Abmachung Termin Installation (Schulung) - DE");
		template.setContent("Guten Tag\n" +
				"\n" +
				"Wir haben neue Funktionen in GinaWeb und möchten diese in Ihrer Testumgebung installieren.\n" +
				"\n" +
				"Es handelt sich um die folgenden Anwendungen:\n" +
				"\n" +
				"- Insassenverwaltung (Modul)\n" +
				"    - Adresseingabe\n" +
				"- Eintritt (Prozess)\n" +
				"    - Röntgenaufnahme\n" +
				"\n" +
				"Wäre der 01.01.2019 für die Aktualisierung der Testumgebung geeignet?\n" +
				"\n" +
				"Wir werden Sie informieren, sobald die Installation abgeschlossen ist und die neuen Funktionen getestet werden können.");
		templates.add(template);

		template = new Template();
		template.setTitle("Abmachung Termin Installation (Schulung) - FR");
		template.setContent("Bonjour\n" +
				"\n" +
				"Nous avons de nouvelles fonctionnalités dans GinaWeb et aimerions les installer dans votre environnement de test.\n" +
				"\n" +
				"Les applications suivantes sont concernées :\n" +
				"\n" +
				"- Gestion des détenus (module)\n" +
				"    - Entrer l'adresse\n" +
				"- Entrée (processus)\n" +
				"    - Photo avec rayons X\n" +
				"\n" +
				"Le 01.01.2019 serait-il adapté à la mise à jour de l'environnement de test ?\n" +
				"\n" +
				"Nous vous informerons dès que l'installation sera terminée et que les nouvelles fonctionnalités pourront être testées.");
		templates.add(template);

		template = new Template();
		template.setTitle("Bestätigung Installation (Schulung) - DE");
		template.setContent("Guten Tag\n" +
				"\n" +
				"Wir konnten die Installation erfolgreich abschließen und Sie können nun die neuen Funktionen testen:\n" +
				"\n" +
				"- Insassenverwaltung (Modul)\n" +
				"    - Adresseingabe\n" +
				"- Eintritt (Prozess)\n" +
				"    - Röntgenaufnahme\n" +
				"\n" +
				"Sobald Sie uns das OK geben, werden wir die Installation in der Produktion planen.\n" +
				"\n" +
				"Bei Problemen kontaktieren Sie uns bitte umgehend.");
		templates.add(template);

		template = new Template();
		template.setTitle("Bestätigung Installation (Schulung) - FR");
		template.setContent("Bonjour\n" +
				"\n" +
				"Nous avons pu terminer l'installation avec succès et vous pouvez maintenant tester les nouvelles fonctionnalités :\n" +
				"\n" +
				"- Gestion des détenus (module)\n" +
				"    - Entrer l'adresse\n" +
				"- Entrée (processus)\n" +
				"    - Photo avec rayons X\n" +
				"\n" +
				"Dès que vous nous aurez donné le feu vert, nous planifierons l'installation en production.\n" +
				"\n" +
				"En cas de problème, veuillez nous contacter immédiatement.");
		templates.add(template);

		template = new Template();
		template.setTitle("Abmachung Termin Installation (Produktion) - DE");
		template.setContent("Guten Tag\n" +
				"\n" +
				"Für folgende Applikationen haben wir das Test OK erhalten:\n" +
				"\n" +
				"- Insassenverwaltung (Modul)\n" +
				"    - Adresseingabe\n" +
				"- Eintritt (Prozess)\n" +
				"    - Röntgenaufnahme\n" +
				"\n" +
				"Wäre der 01.01.2019 für die Aktualisierung der Produktivumgebung geeignet?\n" +
				"\n" +
				"Wir werden Sie informieren, sobald die Installation abgeschlossen ist und Sie GinaWeb wieder benutzen können.");
		templates.add(template);

		template = new Template();
		template.setTitle("Abmachung Termin Installation (Produktion) - FR");
		template.setContent("Bonjour\n" +
				"\n" +
				"Nous avons reçu le test OK pour les applications suivantes :\n" +
				"\n" +
				"- Gestion des détenus (module)\n" +
				"    - Entrer l'adresse\n" +
				"- Entrée (processus)\n" +
				"    - Photo avec rayons X\n" +
				"\n" +
				"Le 01.01.2019 serait-il adapté à la mise à jour de l'environnement de test ?\n" +
				"\n" +
				"Nous vous informerons dès que l'installation sera terminée et vous pourrez utiliser GinaWeb à nouveau.");
		templates.add(template);

		template = new Template();
		template.setTitle("Bestätigung Installation (Produktion) - DE");
		template.setContent("Guten Tag\n" +
				"\n" +
				"Wir konnten die Installation erfolgreich abschließen und Sie können nun GinaWeb wieder benutzen.\n" +
				"\n" +
				"Folgende Applikationen wurden aktualisier:\n" +
				"\n" +
				"- Insassenverwaltung (Modul)\n" +
				"    - Adresseingabe\n" +
				"- Eintritt (Prozess)\n" +
				"    - Röntgenaufnahme\n" +
				"\n" +
				"Bei Problemen kontaktieren Sie uns bitte umgehend.");
		templates.add(template);

		template = new Template();
		template.setTitle("Bestätigung Installation (Produktion) - FR");
		template.setContent("Bonjour\n" +
				"\n" +
				"Nous avons terminé l'installation avec succès et vous pouvez maintenant utiliser GinaWeb à nouveau.\n" +
				"\n" +
				"Les applications suivantes ont été mises à jour :\n" +
				"\n" +
				"- Gestion des détenus (module)\n" +
				"    - Entrer l'adresse\n" +
				"- Entrée (processus)\n" +
				"    - Photo avec rayons X\n" +
				"\n" +
				"En cas de problème, veuillez nous contacter immédiatement.");
		templates.add(template);

		try {
			em.getTransaction().begin();
			for (Template t : templates)
				em.merge(t);
			em.flush();
			em.getTransaction().commit();
			LOGGER.info("Template created");
		} catch (Exception e) {
			LOGGER.error("failed to create templates: ", e);
			if (em.getTransaction() != null && em.getTransaction().isActive())
				em.getTransaction().rollback();
		}
	}

	private void configExportDB(Scanner scanner) throws FileNotFoundException, IOException {

		final Properties propertiesOld = new Properties();
		propertiesOld.loadFromXML(new FileInputStream("src/main/resources/params.xml"));
		System.out.print("do you want so change the default database configuration?\ndefault: " + propertiesOld.getProperty("url") + " [N/y]");
		if (scanner.nextLine().equalsIgnoreCase("y")) {
			System.out.print("Enter jdbc url ONLY MySQL! (jdbc:mysql://localhost:3306/timeTracker): ");
			String jdbcUrl = scanner.nextLine();
			System.out.print("Enter database username: ");
			String user = scanner.nextLine();
			System.out.print("Enter database password: ");
			String pass = scanner.nextLine();

			Properties properties = new Properties();
			properties.put("url", jdbcUrl);
			properties.put("user", user);
			properties.put("password", pass);
			properties.storeToXML(new FileOutputStream("src/main/resources/params.xml"), "");
		}
	}

}
