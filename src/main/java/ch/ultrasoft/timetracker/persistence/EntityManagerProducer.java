package ch.ultrasoft.timetracker.persistence;

import java.io.Serializable;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence; 

@ApplicationScoped
public class EntityManagerProducer implements Serializable { 
	
	private static final long serialVersionUID = 3106757512256211386L;
	private static final EntityManagerFactory factory = Persistence.createEntityManagerFactory("pu"); 
  
	@Produces
	@RequestScoped
	@DataRepository
	public EntityManager produceEntityManager() { 
		return factory.createEntityManager(); 
	}
	
	public void closeEntityManagerFactory() {
		if (factory.isOpen())
			factory.close();
	}
} 