package ch.ultrasoft.timetracker.test.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Ignore;
import org.junit.Test;

import ch.ultrasoft.timetracker.controller.AbstractCrudController;
import ch.ultrasoft.timetracker.model.AbstractEntity;
import ch.ultrasoft.timetracker.utils.BeanUtils;
import ch.ultrasoft.timetracker.utils.MockerUtil;

public abstract class AbstractCrudControllerTest<T1 extends AbstractCrudController, T2 extends AbstractEntity> {

	private final static Logger LOGGER = Logger.getLogger(AbstractCrudControllerTest.class);
	protected T1 controller = (T1) BeanUtils.createAbstractCrudController(getClassOfController());

	private static final int TEST_CASES = 3;

	protected T2 mockEntity() {
			return (T2) MockerUtil.mockRecursiveAndDontSave(getClassOfEntity());// ,
	}

	@Test
	public void saveSelectedTest() {
		for (int i = 0; i < TEST_CASES; i++) {
			controller.setSelectedRecord((AbstractEntity) mockEntity());
			controller.saveSelected();
			assertTrue(controller.getSelectedRecord().getId() != null);
		}
	}

	@SuppressWarnings("unchecked")
	private Class<T1> getClassOfController() {
		return (Class<T1>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}

	@SuppressWarnings("unchecked")
	private Class<T2> getClassOfEntity() {
		return (Class<T2>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[1];
	}

	@Test
	public void readTest() {
		for (int i = 0; i < TEST_CASES; i++) {
			controller.setSelectedRecord((AbstractEntity) mockEntity());
			controller.saveSelected();
		}
		assertTrue(controller.getRecords().size() >= TEST_CASES);
	}

	@Test
	public void deleteTest() {
		for (int i = 0; i < TEST_CASES + 12; i++) {
			controller.setSelectedRecord((AbstractEntity) mockEntity());
			controller.saveSelected();
		}
		List<T2> records = controller.getRecords();
		int sizeBeforeDelete = records.size();
		for (int i = 0; i < TEST_CASES; i++) {
			controller.setSelectedRecord((AbstractEntity) records.get(i));
			controller.deleteSelected();
		}
		assertTrue((controller.getRecords().size() + TEST_CASES) == sizeBeforeDelete);
	}

	@Test
	public void createTest() {
		controller.createRecord();
		assertTrue(controller.getSelectedRecord() != null && controller.getSelectedRecord().getId() == null);
	}

	@Test
	public void converterTestAsObject() {
		for (int i = 0; i < TEST_CASES; i++) {
			controller.setSelectedRecord(mockEntity());
			controller.saveSelected();
			T2 t2_orig = (T2) controller.getRecords().get(i);
			T2 t2_copy =  (T2) controller.getConverter().getAsObject(null, null, t2_orig.getId().toString());
			assertTrue(t2_copy != null && t2_orig != null);
			assertEquals(t2_copy, t2_orig);
		}
	}
	
	@Test
	public void converterTestAsString() {
		for (int i = 0; i < TEST_CASES; i++) {
			controller.setSelectedRecord(mockEntity());
			controller.saveSelected();
			T2 t2_orig = (T2) controller.getRecords().get(i);
			String t2_id_copy = controller.getConverter().getAsString(null, null, t2_orig);
			assertTrue(t2_id_copy != null && !t2_id_copy.isEmpty() && t2_orig != null);
			assertEquals(t2_id_copy, t2_orig.getId().toString());
		}
	}
}