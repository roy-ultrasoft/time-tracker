package ch.ultrasoft.timetracker.utils;

import static org.junit.Assert.*;

import org.junit.Test;

public class CryptoUtilTest {
	
	@Test
	public void hachingTest_000() {	assertEquals(CryptoUtil.toMd5Hash("admin"), "21232f297a57a5a743894a0e4a801fc3");}

	@Test
	public void hachingTest_001() {assertEquals(CryptoUtil.toMd5Hash("abgeschlossen"), "9140d7c839ed75cd6a05854ec7b4c123");}

	@Test
	public void hachingTest_002() {assertEquals(CryptoUtil.toMd5Hash("Adresse"), "50d46c2f993f4c6ab66c095555dcc401");}

	@Test
	public void hachingTest_003() {assertEquals(CryptoUtil.toMd5Hash("Agentur"), "f43dd1ecda3878591d73c853b08330c5");}

	@Test
	public void hachingTest_004() {assertEquals(CryptoUtil.toMd5Hash("aggressiv"), "df023b6b6c47613cf28a75412a23b7e6");}

	@Test
	public void hachingTest_005() {assertEquals(CryptoUtil.toMd5Hash("Aggression"), "3ee3c0683f382094ebc7a67c67a42879");}

	@Test
	public void hachingTest_006() {assertEquals(CryptoUtil.toMd5Hash("akquirieren"), "117e75b32f828a519e3a34377af9491f");}

	@Test
	public void hachingTest_007() {assertEquals(CryptoUtil.toMd5Hash("Akquisition"), "7a32b3a899373b57fe184fc2b94493f4");}

	@Test
	public void hachingTest_008() {assertEquals(CryptoUtil.toMd5Hash("Akquise"), "4e40f17df4e227ee912431ebb54735c6");}

	@Test
	public void hachingTest_009() {assertEquals(CryptoUtil.toMd5Hash("Akupunktur"), "6d3bcbd401bc663d3ea13f8488a7f292");}

	@Test
	public void hachingTest_0010() {assertEquals(CryptoUtil.toMd5Hash("akustisch"), "46ae448179ae6f8e2f0a42c5aec4f067");}

	@Test
	public void hachingTest_0011() {assertEquals(CryptoUtil.toMd5Hash("am besten"), "24573d97413735b5e03045a1a1dd6db0");}

	@Test
	public void hachingTest_0012() {assertEquals(CryptoUtil.toMd5Hash("Ammenhai"), "6cc6a7e97d7586f578da254bc2174eab");}

	@Test
	public void hachingTest_0013() {assertEquals(CryptoUtil.toMd5Hash("Amurtiger"), "a32ab93e31694c36c5efc5fb63c4a157");}

	@Test
	public void hachingTest_0014() {assertEquals(CryptoUtil.toMd5Hash("angsterfuellt"), "bcd93b56d556b616cba054aadb1f5c6f");}

	@Test
	public void hachingTest_0015() {assertEquals(CryptoUtil.toMd5Hash("anhand"), "a591293c5e7661f1c2ae03aa39c57426");}

	@Test
	public void hachingTest_0016() {assertEquals(CryptoUtil.toMd5Hash("anhand"), "a591293c5e7661f1c2ae03aa39c57426");}

	@Test
	public void hachingTest_0017() {assertEquals(CryptoUtil.toMd5Hash("Annalen"), "800e9842a676b06a4069947bd5231044");}

	@Test
	public void hachingTest_0018() {assertEquals(CryptoUtil.toMd5Hash("annullieren"), "cf8403cbd06aaa3e70c7cdebc6f4375c");}

	@Test
	public void hachingTest_0019() {assertEquals(CryptoUtil.toMd5Hash("Anschluss"), "360a29b179c18f066a0d2fe96cd0cdb1");}

	@Test
	public void hachingTest_0020() {assertEquals(CryptoUtil.toMd5Hash("Antipathie"), "fe8a7ee98337cf94922ddca56568ebc0");}

	@Test
	public void hachingTest_0021() {assertEquals(CryptoUtil.toMd5Hash("Apartheid"), "41d886787de3a2f4ac920ee7cb8a862e");}

	@Test
	public void hachingTest_0022() {assertEquals(CryptoUtil.toMd5Hash("Apparat"), "ec6eda7d48fda1d301fa88e213d5b15c");}

	@Test
	public void hachingTest_0023() {assertEquals(CryptoUtil.toMd5Hash("Aergernis"), "9e8cae1efbe1baa0aa26f509c2bbf999");}

	@Test
	public void hachingTest_0024() {assertEquals(CryptoUtil.toMd5Hash("Armatur"), "a1a244644eda116c36ef130f1da2c35e");}

	@Test
	public void hachingTest_0025() {assertEquals(CryptoUtil.toMd5Hash("Arzt"), "6f5e4a64ebe69a01b775bf7b5b66e70c");}

	@Test
	public void hachingTest_0026() {assertEquals(CryptoUtil.toMd5Hash("asozial"), "f79930db203ca7d8188f65ad327de7b4");}

	@Test
	public void hachingTest_0027() {assertEquals(CryptoUtil.toMd5Hash("Astralkoerper"), "1b7a4752469fa88748d86f2ecf2bcb52");}

	@Test
	public void hachingTest_0028() {assertEquals(CryptoUtil.toMd5Hash("asymmetrisch"), "639ddba85bc0e625f1580a0b9459e304");}

	@Test
	public void hachingTest_0029() {assertEquals(CryptoUtil.toMd5Hash("Asymmetrie"), "333616ddff66859ddcbf7c2be5a1e028");}

	@Test
	public void hachingTest_0030() {assertEquals(CryptoUtil.toMd5Hash("Atmosphaere"), "6ef3a5f61181283109f46549b379c715");}

	@Test
	public void hachingTest_0031() {assertEquals(CryptoUtil.toMd5Hash("Attrappe"), "d154318ed6aa1318cdc1491c52c622d1");}

	@Test
	public void hachingTest_0032() {assertEquals(CryptoUtil.toMd5Hash("auf einmal"), "231a4892ef79bcc52c5327a6e0ffcff4");}

	@Test
	public void hachingTest_0033() {assertEquals(CryptoUtil.toMd5Hash("aufgrund dessen"), "661618a4e708f4286395f49092ef313e");}

}
