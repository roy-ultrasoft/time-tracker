package ch.ultrasoft.timetracker.utils;

import java.lang.reflect.Field;

import javax.persistence.EntityManager;

import org.apache.log4j.Logger;

import ch.ultrasoft.timetracker.beans.UserBean;
import ch.ultrasoft.timetracker.controller.AbstractCrudController;
import ch.ultrasoft.timetracker.converter.AbstractEntityConverter;
import ch.ultrasoft.timetracker.model.AbstractEntity;
import ch.ultrasoft.timetracker.model.User;
import ch.ultrasoft.timetracker.persistence.EntityManagerProducer;
import ch.ultrasoft.timetracker.service.GenericService;

public class BeanUtils {

	private static final Logger LOGGER = Logger.getLogger(BeanUtils.class);

	public static AbstractCrudController createAbstractCrudController(Class<? extends AbstractCrudController> clazz) {
		Field service4Bean;
		Field converter4Bean;
		Field userBean4Bean;
		try {
			service4Bean = clazz.getSuperclass().getDeclaredField("service");
			service4Bean.setAccessible(true);
			converter4Bean = clazz.getSuperclass().getDeclaredField("converter");
			converter4Bean.setAccessible(true);
			userBean4Bean = clazz.getSuperclass().getDeclaredField("userBean");
			userBean4Bean.setAccessible(true);
			
			EntityManager em = new EntityManagerProducer().produceEntityManager();
			
			AbstractCrudController crudController = clazz.newInstance();
			UserBean userBean = createUserBean(em);
			GenericService genericService = createGenericService(service4Bean.getType(), userBean, em);
			AbstractEntityConverter<AbstractEntity> genericKeyableConverter = createGenericConverter(converter4Bean.getType(), genericService);
            
			service4Bean.set(crudController, genericService);
			converter4Bean.set(crudController, genericKeyableConverter);
			userBean4Bean.set(crudController, userBean);

			return crudController;
		} catch (NoSuchFieldException | SecurityException e) {
			e.printStackTrace();
		} catch (InstantiationException e1) {
			e1.printStackTrace();
		} catch (IllegalAccessException e1) {
			e1.printStackTrace();
		}
		return null;
	}
	
	private static UserBean createUserBean(EntityManager em) {
		UserBean userBean = new UserBean();
		userBean.setUser(MockerUtil.mockRecursiveAndSave(User.class, em));
		return userBean;
	}

	@SuppressWarnings("rawtypes")
	public static GenericService createGenericService(Class<?> clazz, UserBean userBean, EntityManager em) {
		Field em4Service;
		Field userBean4Service;
		GenericService genericService;
		try {
			genericService = (GenericService) clazz.newInstance();
			try {
				em4Service = genericService.getClass().getDeclaredField("em");
				em4Service.setAccessible(true);
				em4Service.set(genericService, em);
				userBean4Service = genericService.getClass().getDeclaredField("userBean");
				userBean4Service.setAccessible(true);
				userBean4Service.set(genericService, userBean);
				return genericService;
			} catch (NoSuchFieldException | SecurityException e) {
				e.printStackTrace();
			}
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}
		return null;
	}

	@SuppressWarnings("rawtypes")
	public static AbstractEntityConverter createGenericConverter(Class<?> clazz, GenericService service) {
		Field servies4Converter;
		Field clazz4Converter;
		AbstractEntityConverter<AbstractEntity> genericConverter;
		try {
			genericConverter = (AbstractEntityConverter<AbstractEntity>) clazz.newInstance();
			try {
				servies4Converter = genericConverter.getClass().getDeclaredField("service");
				servies4Converter.setAccessible(true);
				servies4Converter.set(genericConverter, service);
				clazz4Converter = genericConverter.getClass().getDeclaredField("clazz");
				clazz4Converter.setAccessible(true);
				clazz4Converter.set(genericConverter, AbstractEntity.class);
				return genericConverter;
			} catch (NoSuchFieldException | SecurityException e) {
				e.printStackTrace();
			}
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
