package ch.ultrasoft.timetracker.utils;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Date;
import java.util.Random;

public final class RandomUtil {

	public static String getRandomString() {
		return (new BigInteger(130, new SecureRandom()).toString(32));
	}

	public static Integer getRandomInteger() {
		return (int) (Math.random() * Integer.MAX_VALUE) % Integer.MAX_VALUE;
	}

	public static Long getRandomLong() {
		return (long) ((Math.random() * Long.MAX_VALUE) % Long.MAX_VALUE);
	}

	public static Short getRandomShort() {
		return (short) ((short) (Math.random() * Short.MIN_VALUE) % Short.MIN_VALUE);
	}

	public static float getRandomFload() {
		return (float) (Math.random() * Float.MAX_VALUE) % Float.MAX_VALUE;
	}

	public static Double getRandomDouble() {
		return (Math.random() * Double.MAX_VALUE) % Double.MAX_VALUE;
	}

	public static byte getRandomByte() {
		return (byte) ((Math.random() * 1280) % 128);
	}

	public static byte[] getRandomByteArray() {
		return getRandomString().getBytes();
	}

	public static boolean getRandomBoolean() {
		return (int) ((Math.random() * 150) % 2) == 1 ? true : false;
	}
	
	public static Date getRandomDate() {
		// Get an Epoch value roughly between 1940 and 2010
		// -946771200000L = January 1, 1940
		// Add up to 70 years to it (using modulus on the next long)
		Long ms = -946771200000L + (Math.abs(new Random().nextLong()) % (70L * 365 * 24 * 60 * 60 * 1000));

		return new Date(ms);
	}
}
