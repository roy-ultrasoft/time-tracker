package ch.ultrasoft.timetracker.utils;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Date;

import javax.persistence.EntityManager;

import org.apache.log4j.Logger;

public final class MockerUtil {
	private final static Logger LOGGER = Logger.getLogger(MockerUtil.class);

	@SuppressWarnings("finally")
	public static <T> T mockRecursiveAndSave(Class<T> clazz, EntityManager em){
		T t = null;
		em.getTransaction().begin();
		try {
			t = MockerUtil.mockRecursiveAndPersist(clazz, em);
			em.getTransaction().commit();
		} catch (Exception e) {
			LOGGER.error("mockRecursiveAndSave", e);
			if (em.getTransaction().isActive())
				em.getTransaction().rollback();
		} finally {
			return t;
		}
	}
	
	@SuppressWarnings("finally")
	public static <T> T mockRecursiveAndDontSave(Class<T> clazz){
		T t = null;
		try {
			t = MockerUtil.mockRecursiveAndPersist(clazz, null);
		} catch (Exception e) {
			LOGGER.error("mockRecursiveAndDontSave", e);
		} finally {
			return t;
		}
	}
	
	private static <T> T mockRecursiveAndPersist(Class<T> clazz, EntityManager em) throws Exception {
		T t = null;
		try {
			t = (T) clazz.newInstance();
		} catch (Exception e) {
			return null;
		}
		Field[] fields = t.getClass().getDeclaredFields();
		if (fields != null)
			for (Field f : fields)
				if (!Modifier.isFinal(f.getModifiers()) && !Modifier.isInterface(f.getModifiers()) && !Modifier.isAbstract(f.getModifiers())) {
					f.setAccessible(true);
					try {
						if (f.get(t) == null) {
							// Donot set value to id
							if (f.getName().equalsIgnoreCase("id"))
								continue;
							// Integers Random
							else if (f.getType().equals(Integer.class) || f.getType().equals(int.class))
								f.set(t ,0);
							// Double Random
							else if (f.getType().equals(Double.class) || f.getType().equals(double.class))
								f.set(t , RandomUtil.getRandomInteger());
							// Long Random
							else if (f.getType().equals(Long.class) || f.getType().equals(long.class))
								f.set(t , RandomUtil.getRandomLong());
							// Float Random
							else if (f.getType().equals(Float.class) || f.getType().equals(float.class))
								f.set(t ,RandomUtil.getRandomFload());
							// Short Random
							else if (f.getType().equals(Short.class) || f.getType().equals(short.class))
								f.set(t ,RandomUtil.getRandomShort());
							// ByteArray Random
							else if (f.getType().equals(Byte[].class) || f.getType().equals(byte[].class))
								f.set(t ,RandomUtil.getRandomByteArray());
							// Byte Random
							else if (f.getType().equals(Byte.class) || f.getType().equals(byte.class))
								f.set(t , RandomUtil.getRandomByte());
							// Sting randomizer : todo range dynamic
							else if (f.getType().equals(String.class))
								f.set(t ,RandomUtil.getRandomString());
							// Boolean
							else if (f.getType().equals(Boolean.class) || f.getType().equals(boolean.class))
								f.set(t ,RandomUtil.getRandomBoolean());
							// date
							else if (f.getType().equals(Date.class))
								f.set(t ,RandomUtil.getRandomDate());
							else if (t.getClass().getPackage().toString().contains("ch.ultrasoft.timetracker.model"))
								f.set(t ,mockRecursiveAndPersist(f.getType(), em));
							else 
								t = null;
							if (t != null && em != null && t.getClass().getPackage().toString().contains("ch.ultrasoft.timetracker.model"))
								t = em.merge(t);
						}
					} catch (Exception e) {
						LOGGER.error("mockRecursiveAndPersist: ", e);
					}
				}
		return t;
	}

	public static <T> T saveAttachedEntities(T t, EntityManager em) {
		for (Field f : t.getClass().getDeclaredFields()) {
			if (f.getType().getPackage() != null && f.getType().getPackage().toString().contains("ch.ultrasoft.timetracker.model")){
				try {
					f.setAccessible(true);
					f.set(t, f.getType().cast(MockerUtil.mockRecursiveAndSave(f.getType(), em)));
				} catch (IllegalArgumentException | IllegalAccessException e) {
					LOGGER.error("saveAttachedEntities:" + e);
				}
			}
		}
		return t;
	}
}
