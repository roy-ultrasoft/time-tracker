 package ch.ultrasoft.timetracker.persistence;

import static org.junit.Assert.assertTrue;

import javax.persistence.EntityManager;

import org.apache.log4j.Logger;
import org.junit.Test;

import ch.ultrasoft.timetracker.persistence.EntityManagerProducer;

public class PersistenceTest {

	private final static Logger LOGGER = Logger.getLogger(PersistenceTest.class);

	private EntityManager em = new EntityManagerProducer().produceEntityManager();
	
	@Test
	public void testEm() {
		assertTrue(em != null);
	}

	@Test
	public void testTransaction() {
		em.getTransaction().begin();
		try {
			assertTrue(em.getTransaction().isActive());
			em.getTransaction().commit();
		} catch (Exception e) {
			if (em.getTransaction().isActive())
				em.getTransaction().rollback();
			assertTrue(false);
		}
	}
}
