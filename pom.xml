<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>ch.ultrasoft</groupId>
	<artifactId>time-tracker</artifactId>
	<version>17.0.0</version>
	<name>time-tracker</name>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>

		<jdk.version>1.7</jdk.version>
		<org.apache.maven.plugins.compiler>2.3.2</org.apache.maven.plugins.compiler>
		<javax.el.version>3.0.0</javax.el.version>
		<javax.jstl.version>1.2</javax.jstl.version>
		<javax.servlet-api.version>3.1.0</javax.servlet-api.version>
		<javax.validation-api.version>1.1.0.Final</javax.validation-api.version>
		<javax.javaee-web-api.version>7.0</javax.javaee-web-api.version>


		<jsf.version>2.2.13</jsf.version>
		<weld.servlet.version>2.3.4.Final</weld.servlet.version>

		<junit.version>4.12</junit.version>
		<mockito.version>1.9.5</mockito.version>
		
		<slf4j-api.version>1.7.21</slf4j-api.version>
		<log4j.version>1.2.17</log4j.version>

		<org.hibernate.version>5.1.0.Final</org.hibernate.version>
		<com.h2database.version>1.3.158</com.h2database.version>
		<mysql.jdbc.version>5.1.34</mysql.jdbc.version>

		<primefaces.version>6.0</primefaces.version>
		<primefaces.extensions.version>6.0.0</primefaces.extensions.version>
		<primefaces.extensions.themes.version>1.0.10</primefaces.extensions.themes.version>
		
		<org.apache.tomcat.maven.plugin.version>2.2</org.apache.tomcat.maven.plugin.version>
		<tomcat.path>/time-tracker</tomcat.path>
		<tomcat.port>8080</tomcat.port>

	</properties>

	<dependencies>

		<!-- JUnit -->
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>${junit.version}</version>
			<scope>test</scope>
		</dependency>

		<!-- Mockito -->
		<dependency>
			<groupId>org.mockito</groupId>
			<artifactId>mockito-all</artifactId>
			<version>${mockito.version}</version>
			<scope>test</scope>
		</dependency>
		
		<!-- JSF Dependencies -->
		<dependency>
			<groupId>javax.servlet</groupId>
			<artifactId>jstl</artifactId>
			<version>${javax.jstl.version}</version>
		</dependency>

		<dependency>
			<groupId>com.sun.faces</groupId>
			<artifactId>jsf-api</artifactId>
			<version>${jsf.version}</version>
			<scope>compile</scope>
		</dependency>

		<dependency>
			<groupId>com.sun.faces</groupId>
			<artifactId>jsf-impl</artifactId>
			<version>${jsf.version}</version>
			<scope>compile</scope>
		</dependency>

		<dependency>
			<groupId>javax.el</groupId>
			<artifactId>javax.el-api</artifactId>
			<version>${javax.el.version}</version>
			<scope>provided</scope>
		</dependency>

		<!-- CDI Dependencies -->
		<dependency>
			<groupId>org.jboss.weld.servlet</groupId>
			<artifactId>weld-servlet</artifactId>
			<version>${weld.servlet.version}</version>
			<scope>compile</scope>
		</dependency>

		<!-- Bean Validation Dependencies -->
		<dependency>
			<groupId>javax.validation</groupId>
			<artifactId>validation-api</artifactId>
			<version>${javax.validation-api.version}</version>
		</dependency>


		<dependency>
			<groupId>javax.servlet</groupId>
			<artifactId>javax.servlet-api</artifactId>
			<version>${javax.servlet-api.version}</version>
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>javax</groupId>
			<artifactId>javaee-web-api</artifactId>
			<version>${javax.javaee-web-api.version}</version>
			<scope>provided</scope>
		</dependency>

		<!-- JPA Dependencies -->
		<dependency>
			<groupId>org.hibernate</groupId>
			<artifactId>hibernate-core</artifactId>
			<version>${org.hibernate.version}</version>
		</dependency>

		<dependency>
			<groupId>org.hibernate</groupId>
			<artifactId>hibernate-entitymanager</artifactId>
			<version>${org.hibernate.version}</version>
		</dependency>

		<!-- DB -->

		<dependency>
			<groupId>com.h2database</groupId>
			<artifactId>h2</artifactId>
			<version>${com.h2database.version}</version>
		</dependency>

		<dependency>
			<groupId>mysql</groupId>
			<artifactId>mysql-connector-java</artifactId>
			<version>${mysql.jdbc.version}</version>
		</dependency>

		<!-- Primefaces -->
		<dependency>
			<groupId>org.primefaces</groupId>
			<artifactId>primefaces</artifactId>
			<version>${primefaces.version}</version>
		</dependency>

		<dependency>
			<groupId>org.primefaces.extensions</groupId>
			<artifactId>primefaces-extensions</artifactId>
			<version>${primefaces.extensions.version}</version>
		</dependency>
		
		<!-- Logging -->
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-api</artifactId>
			<version>${slf4j-api.version}</version>
		</dependency>

		<dependency>
			<groupId>log4j</groupId>
			<artifactId>log4j</artifactId>
			<version>${log4j.version}</version>
		</dependency>

	</dependencies>

	<repositories>
		<!-- JBoss Repository used for Java EE 6 pieces -->
		<repository>
			<id>repository.jboss.org</id>
			<name>JBoss Repository</name>
			<url>http://repository.jboss.org/nexus/content/groups/public-jboss/</url>
		</repository>

		<!-- Primefaces -->
		<repository>
			<url>http://repository.primefaces.org/</url>
			<id>PrimeFaces-maven-lib</id>
			<layout>default</layout>
			<name>Repository for library PrimeFaces-maven-lib</name>
		</repository>
	</repositories>

	<build>

		<plugins>
			<!-- "Embedded tomtcat" $ mvn clean install tomcat7:run-war -->
			<plugin>
				<groupId>org.apache.tomcat.maven</groupId>
				<artifactId>tomcat7-maven-plugin</artifactId>
				<version>${org.apache.tomcat.maven.plugin.version}</version>
				<configuration>
					<port>${tomcat.port}</port>
					<path>${tomcat.path}</path>
				</configuration>
			</plugin>

			<!-- Define JDK Version -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>${org.apache.maven.plugins.compiler}</version>
				<configuration>
					<source>${jdk.version}</source>
					<target>${jdk.version}</target>
				</configuration>
			</plugin>
			
			<!-- "Creates a jar that can be run as time-tracker weapp"  
			$ java -jar time-tracker.jar -httpPort=7070 --> 
			<plugin> 
				<groupId>org.apache.tomcat.maven</groupId> 
				<artifactId>tomcat7-maven-plugin</artifactId> 
				<version>2.0</version> 
				<executions> 
					<execution> 
						<id>tomcat-run</id> 
						<goals> 
							<goal>exec-war-only</goal> 
						</goals> 
						<phase>package</phase> 
						<configuration> 
							<path>${tomcat.path}</path> 
							<enableNaming>false</enableNaming> 
							<finalName>time-tracker##${project.version}.jar</finalName>
							<charset>utf-8</charset>
						</configuration> 
					</execution> 
				</executions> 
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-plugin</artifactId>
				<configuration>
					<useSystemClassLoader>false</useSystemClassLoader>
				</configuration>
			</plugin>
		</plugins>
	</build>

	<packaging>war</packaging>
</project>
